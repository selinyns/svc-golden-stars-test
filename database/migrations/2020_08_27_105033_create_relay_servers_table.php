<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRelayServersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('relay_servers', function(Blueprint $table)
		{
			$table->string('type', 10)->unique();
			$table->string('ip', 15)->nullable();
			$table->integer('port')->nullable()->index('idx_port');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('relay_servers');
	}

}
