<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMelmagaHistoryLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('melmaga_history_logs', function(Blueprint $table)
		{
			$table->integer('melmaga_id')->index('idx_melmaga_id');
			$table->integer('client_id')->index('idx_client_id');
			$table->integer('read_flg')->default(0)->index('idx_read_flg');
			$table->dateTime('first_view_datetime')->nullable();
			$table->bigInteger('sort_date')->unsigned()->index('idx_sort_date');
			$table->timestamps();
			$table->unique(['melmaga_id','client_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('melmaga_history_logs');
	}

}
