<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonalAccessLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('personal_access_logs', function(Blueprint $table)
		{
			$table->string('login_id')->index('idx_ad_cd');
			$table->integer('melmaga_id')->nullable()->index('idx_melmaga_id');
			$table->string('page')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personal_access_logs');
	}

}
