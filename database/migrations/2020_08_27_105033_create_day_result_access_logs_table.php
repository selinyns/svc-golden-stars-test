<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDayResultAccessLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('day_result_access_logs', function(Blueprint $table)
		{
			$table->integer('access_date')->unique();
			$table->integer('no_pay');
			$table->integer('pay');
			$table->integer('total');
			$table->integer('no_pay24');
			$table->integer('pay24');
			$table->integer('total24');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('day_result_access_logs');
	}

}
