<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConvertTablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('convert_tables', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('type')->unsigned()->default(0)->index('idx_type');
			$table->string('key', 100)->index('idx_key');
			$table->string('value')->nullable();
			$table->string('memo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('convert_tables');
	}

}
