<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 100);
			$table->text('contents', 65535)->nullable();
			$table->integer('sort')->default(1)->index('idx_sort');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contents');
	}

}
