<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandingPagesPreviewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('landing_pages_previews', function(Blueprint $table)
		{
			$table->integer('lp_id')->index('idx_lp_id');
			$table->integer('type')->default(0)->index('idx_type');
			$table->string('name', 20)->index('idx_name');
			$table->text('content', 65535)->nullable();
			$table->timestamps();
			$table->unique(['lp_id','name']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landing_pages_previews');
	}

}
