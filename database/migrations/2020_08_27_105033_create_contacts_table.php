<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('contacts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->index('idx_client_id');
			$table->string('email')->index('idx_email');
			$table->dateTime('reply_date')->nullable()->index('idx_reply_date');
			$table->integer('group_id')->nullable()->index('idx_group_id');
			$table->boolean('status')->index('idx_status');
			$table->integer('read_flg')->default(0)->index('idx_read_flg');
			$table->string('subject', 50)->index('idx_subject');
			$table->string('msg', 300);
			$table->string('memo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contacts');
	}

}
