<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegisteredMailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('registered_mails', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('specified_time')->index('idx_specified_time');
			$table->integer('enable_flg')->index('idx_enable_flg');
			$table->integer('item_type')->nullable()->index('idx_item_type');
			$table->string('item_value')->nullable()->index('idx_item_value');
			$table->integer('like_type')->nullable()->index('idx_like_type');
			$table->text('groups', 65535)->nullable();
			$table->integer('device')->nullable()->index('idx_device');
			$table->string('title')->index('idx_title');
			$table->text('body', 65535)->nullable();
			$table->text('html_body', 65535)->nullable();
			$table->text('remarks', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registered_mails');
	}

}
