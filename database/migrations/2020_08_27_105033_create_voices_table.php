<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVoicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('voices', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('open_flg')->default(0)->index('idx_open_flg');
			$table->string('title', 100)->nullable()->index('idx_title');
			$table->string('name', 100)->nullable()->index('idx_name');
			$table->string('msg')->nullable();
			$table->string('img')->nullable();
			$table->date('post_date')->nullable()->index('idx_post_date');
			$table->date('sort_date')->index('idx_sort_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('voices');
	}

}
