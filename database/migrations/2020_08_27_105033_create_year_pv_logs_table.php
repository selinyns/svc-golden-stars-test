<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateYearPvLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('year_pv_logs', function(Blueprint $table)
		{
			$table->string('id', 32);
			$table->integer('access_date')->index('idx_access_date');
			$table->string('url')->index('idx_url');
			$table->integer('total')->index('idx_total');
			$table->timestamps();
			$table->unique(['access_date','url']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('year_pv_logs');
	}

}
