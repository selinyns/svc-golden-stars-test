<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResultAdLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('result_ad_logs', function(Blueprint $table)
		{
			$table->string('ad_cd')->index('idx_ad_cd');
			$table->integer('access_date');
			$table->integer('pv')->index('idx_pv');
			$table->integer('temp_reg')->index('idx_temp_reg');
			$table->integer('reg')->index('idx_reg');
			$table->integer('quit')->index('idx_quit');
			$table->integer('active')->index('idx_active');
			$table->integer('order_num')->index('idx_order_num');
			$table->integer('pay')->index('idx_pay');
			$table->integer('amount')->index('idx_amount');
			$table->timestamps();
			$table->unique(['ad_cd','access_date']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('result_ad_logs');
	}

}
