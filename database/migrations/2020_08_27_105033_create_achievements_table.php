<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAchievementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('achievements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('type')->default(0);
			$table->integer('product_id')->unsigned()->nullable()->index('idx_product_id');
			$table->integer('priority_id')->default(1)->index('idx_priority_id');
			$table->integer('open_flg')->default(0)->index('idx_open_flg');
			$table->date('race_date')->index('idx_race_date');
			$table->string('race_name', 100)->index('idx_race_name');
			$table->string('msg1')->nullable();
			$table->string('msg2')->nullable();
			$table->string('msg3')->nullable();
			$table->string('memo')->nullable();
			$table->date('sort_date')->index('idx_sort_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('achievements');
	}

}
