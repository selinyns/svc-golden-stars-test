<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('login_id', 254)->nullable()->unique('login_id');
			$table->string('password')->nullable();
			$table->string('password_raw')->nullable();
			$table->string('mail_address')->nullable()->index('idx_mail_address');
			$table->string('mobile_mail_address')->nullable()->unique('mobile_mail_address');
			$table->string('identify_id', 10)->nullable();
			$table->string('session_id', 20)->nullable();
			$table->string('remember_token', 100)->nullable()->index('idx_remember_token');
			$table->boolean('mail_status')->default(1)->index('idx_mail_status');
			$table->boolean('status')->default(0)->index('idx_status');
			$table->integer('point')->unsigned()->default(0)->index('idx_point');
			$table->integer('pay_count')->default(0)->index('idx_pay_count');
			$table->integer('pay_amount')->default(0)->index('idx_pay_amount');
			$table->integer('group_id')->default(0)->index('idx_group_id');
			$table->string('ad_cd')->nullable()->default('')->index('idx_ad_cd');
			$table->string('credit_certify_phone_no')->nullable();
			$table->boolean('bonus_flg')->nullable()->default(0)->index('idx_bonus_flg');
			$table->integer('action')->nullable()->default(0)->index('idx_action');
			$table->dateTime('pay_datetime')->nullable()->index('idx_pay_datetime');
			$table->bigInteger('sort_pay_datetime')->nullable()->index('idx_sort_pay_datetime');
			$table->dateTime('last_access_datetime')->nullable()->index('idx_last_access_datetime');
			$table->bigInteger('sort_last_access_datetime')->nullable()->index('idx_sort_last_access_datetime');
			$table->dateTime('quit_datetime')->nullable()->index('idx_quit_datetime');
			$table->bigInteger('sort_quit_datetime')->nullable()->index('idx_sort_quit_datetime');
			$table->dateTime('temporary_datetime')->nullable()->index('idx_temporary_datetime');
			$table->bigInteger('sort_temporary_datetime')->nullable()->index('idx_sort_temporary_datetime');
			$table->text('description', 65535)->nullable();
			$table->bigInteger('regist_date')->nullable()->index('idx_regist_date');
			$table->timestamps();
			$table->boolean('disable')->default(0)->index('idx_disable');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
