<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMagnificationSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('magnification_settings', function(Blueprint $table)
		{
			$table->string('type', 20)->unique();
			$table->integer('default_id')->index('idx_default_id');
			$table->integer('category_id')->index('idx_category_id');
			$table->dateTime('start_date')->nullable()->index('idx_start_date');
			$table->dateTime('end_date')->nullable()->index('idx_end_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('magnification_settings');
	}

}
