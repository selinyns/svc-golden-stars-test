<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteoRelayServersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mysql')->create('relay_servers', function(Blueprint $table)
		{
			$table->string('type', 10)->index('idx_type');
			$table->string('ip', 15)->nullable();
			$table->integer('port')->nullable()->index('idx_port');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('relay_servers');
	}

}
