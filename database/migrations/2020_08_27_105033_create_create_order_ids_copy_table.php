<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreateOrderIdsCopyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('create_order_ids_copy', function(Blueprint $table)
		{
			$table->increments('order_id');
			$table->string('key', 20)->default('product')->unique('create_order_ids_key_unique');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('create_order_ids_copy');
	}

}
