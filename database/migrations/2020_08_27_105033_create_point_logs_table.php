<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePointLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('point_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('login_id')->index('idx_login_id');
			$table->integer('add_point')->index('idx_add_point');
			$table->integer('prev_point')->unsigned()->index('idx_prev_point');
			$table->integer('current_point')->unsigned()->index('idx_current_point');
			$table->string('operator')->index('idx_operator');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('point_logs');
	}

}
