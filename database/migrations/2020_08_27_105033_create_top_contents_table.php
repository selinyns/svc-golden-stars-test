<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('top_contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title')->nullable()->index('idx_title');
			$table->integer('type')->default(1)->index('idx_type');
			$table->integer('open_flg')->default(0)->index('idx_open_flg');
			$table->integer('link_flg')->default(0)->index('idx_link_flg');
			$table->string('groups')->nullable();
			$table->integer('order_num')->default(1)->index('idx_order');
			$table->dateTime('start_date')->nullable()->index('idx_start_date');
			$table->bigInteger('sort_start_date')->nullable()->index('idx_sort_start_date');
			$table->dateTime('end_date')->nullable()->index('idx_end_date');
			$table->bigInteger('sort_end_date')->nullable()->index('idx_sort_end_date');
			$table->string('url')->nullable();
			$table->string('img')->nullable();
			$table->text('html_body', 65535)->nullable();
			$table->date('sort_date')->nullable()->index('idx_sort_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('top_contents');
	}

}
