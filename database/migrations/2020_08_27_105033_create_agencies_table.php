<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAgenciesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('agencies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 50);
			$table->string('login_id')->index('idx_login_id');
			$table->string('password')->nullable();
			$table->string('password_raw')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->string('memo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('agencies');
	}

}
