<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDayPvLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('day_pv_logs', function(Blueprint $table)
		{
			$table->string('ad_cd')->index('idx_ad_cd');
			$table->string('login_id')->nullable()->index('idx_login_id');
			$table->integer('access_date')->index('idx_access_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('day_pv_logs');
	}

}
