<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandingPagesContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('landing_pages_contents', function(Blueprint $table)
		{
			$table->integer('lp_id')->index('idx_lp_id');
			$table->integer('type')->default(0)->index('idx_type');
			$table->integer('url_open_flg')->default(0);
			$table->string('name', 20);
			$table->text('content', 65535)->nullable();
			$table->timestamps();
			$table->unique(['lp_id','name','type'], 'landing_pages_contents_lp_id_name_unique');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landing_pages_contents');
	}

}
