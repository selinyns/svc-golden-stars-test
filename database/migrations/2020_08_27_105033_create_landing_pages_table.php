<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLandingPagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('landing_pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('open_flg')->default(0);
			$table->string('memo')->nullable();
			$table->text('img', 65535)->nullable();
			$table->date('sort_date')->index('idx_sort_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('landing_pages');
	}

}
