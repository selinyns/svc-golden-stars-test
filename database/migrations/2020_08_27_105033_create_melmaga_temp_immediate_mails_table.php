<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMelmagaTempImmediateMailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('melmaga_temp_immediate_mails', function(Blueprint $table)
		{
			$table->integer('melmaga_id')->index('idx_melmaga_id');
			$table->integer('client_id')->index('idx_client_id');
			$table->integer('success_flg')->nullable();
			$table->timestamps();
			$table->unique(['melmaga_id','client_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('melmaga_temp_immediate_mails');
	}

}
