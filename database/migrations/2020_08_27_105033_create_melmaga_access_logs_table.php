<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMelmagaAccessLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('melmaga_access_logs', function(Blueprint $table)
		{
			$table->integer('melmaga_id')->index('idx_melmaga_id');
			$table->string('login_id')->index('idx_login_id');
			$table->integer('access_date')->index('idx_access_date');
			$table->timestamps();
			$table->unique(['melmaga_id','login_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('melmaga_access_logs');
	}

}
