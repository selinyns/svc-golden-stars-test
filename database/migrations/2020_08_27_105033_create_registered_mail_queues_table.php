<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegisteredMailQueuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('registered_mail_queues', function(Blueprint $table)
		{
			$table->integer('send_id');
			$table->string('ad_cd')->nullable()->index('idx_ad_cd');
			$table->integer('client_id')->index('idx_client_id');
			$table->string('mail')->index('idx_mail');
			$table->integer('group_id')->nullable()->index('idx_group_id');
			$table->integer('device')->index('idx_device');
			$table->timestamps();
			$table->unique(['send_id','mail'], 'send_id_mail_unique');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('registered_mail_queues');
	}

}
