<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGrantPointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('grant_points', function(Blueprint $table)
		{
			$table->string('type', 20)->unique();
			$table->integer('point');
			$table->text('dispmsg', 65535)->nullable();
			$table->integer('disptime')->nullable();
			$table->integer('disp_flg')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('grant_points');
	}

}
