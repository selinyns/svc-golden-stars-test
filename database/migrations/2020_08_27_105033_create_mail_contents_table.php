<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMailContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('mail_contents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->string('from', 50);
			$table->string('from_mail');
			$table->string('subject', 100);
			$table->text('body', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('mail_contents');
	}

}
