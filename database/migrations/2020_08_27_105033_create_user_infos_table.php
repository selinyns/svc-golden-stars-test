<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserInfosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('user_infos', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('userinfo', 65535);
			$table->integer('order')->default(0)->index('idx_order');
			$table->integer('disptime')->default(0);
			$table->integer('disp_flg')->default(0)->index('idx_disp_flg');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_infos');
	}

}
