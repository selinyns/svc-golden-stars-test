<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePointsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('points', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('pay_type', 20)->index('idx_pay_type');
			$table->integer('money')->index('idx_money');
			$table->integer('point');
			$table->string('disp_msg')->nullable();
			$table->string('memo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('points');
	}

}
