<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMonthPvLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('month_pv_logs', function(Blueprint $table)
		{
			$table->string('id', 32);
			$table->integer('access_date')->index('idx_access_date');
			$table->string('url')->index('idx_url');
			$table->integer('total')->index('idx_total');
			$table->timestamps();
			$table->unique(['access_date','url']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('month_pv_logs');
	}

}
