<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('ad_codes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('group_id')->nullable();
			$table->string('ad_cd')->index('idx_ad_cd');
			$table->string('single_opt', 1)->default('0')->index('idx_single_opt');
			$table->integer('agency_id')->nullable();
			$table->string('login_id')->nullable();
			$table->string('password')->nullable();
			$table->string('category', 30)->index('idx_category');
			$table->integer('aggregate_flg')->index('idx_aggregate_flg');
			$table->string('name')->nullable()->index('idx_name');
			$table->string('url')->nullable()->index('idx_url');
			$table->string('memo')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ad_codes');
	}

}
