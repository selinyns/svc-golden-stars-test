<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteoAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('mysql')->create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->index('idx_email');
			$table->string('password')->nullable();
			$table->integer('type')->default(0)->index('idx_type');
			$table->string('remember_token', 100)->nullable();
			$table->string('select_db', 20)->nullable();
			$table->string('access_host', 50)->nullable();
			$table->string('user_agent')->nullable();
			$table->dateTime('last_login_date')->nullable()->index('idx_last_login_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
