<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForecastsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('forecasts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->dateTime('disp_sdate')->nullable()->index('idx_disp_sdate');
			$table->dateTime('disp_edate')->nullable()->index('idx_disp_edate');
			$table->dateTime('open_sdate')->nullable()->index('idx_open_sdate');
			$table->dateTime('open_edate')->nullable()->index('idx_open_edate');
			$table->integer('category')->default(1)->index('idx_category');
			$table->string('groups')->nullable()->index('idx_groups');
			$table->integer('product_id')->default(0)->index('idx_product_id');
			$table->integer('campaigns')->nullable()->index('idx_campaigns');
			$table->integer('open_flg')->default(0)->index('idx_open_flg');
			$table->integer('point')->nullable()->default(0);
			$table->string('title', 100)->nullable()->index('idx_title');
			$table->text('headline', 65535)->nullable();
			$table->string('comment')->nullable()->index('idx_comment');
			$table->text('detail', 65535)->nullable();
			$table->integer('visitor')->default(0);
			$table->date('disp_sort_sdate')->nullable()->index('idx_disp_sort_sdate');
			$table->date('disp_sort_edate')->nullable()->index('idx_disp_sort_edate');
			$table->date('open_sort_sdate')->nullable()->index('idx_open_sort_sdate');
			$table->date('open_sort_edate')->nullable()->index('idx_open_sort_edate');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('forecasts');
	}

}
