<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePersonalMailLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('personal_mail_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->index('idx_client_id');
			$table->string('subject', 100);
			$table->text('body', 65535);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('personal_mail_logs');
	}

}
