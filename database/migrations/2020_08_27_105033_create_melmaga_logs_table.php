<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMelmagaLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('melmaga_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('send_status')->index('idx_send_status');
			$table->integer('send_count')->index('idx_send_count');
			$table->string('send_method', 50)->nullable();
			$table->string('from_name', 50);
			$table->string('from_mail');
			$table->string('subject');
			$table->text('text_body', 65535)->nullable();
			$table->text('html_body', 65535)->nullable();
			$table->integer('newspage_flg')->default(1)->index('idx_newspage_flg');
			$table->text('query', 65535)->nullable();
			$table->text('bindings', 65535)->nullable();
			$table->text('items', 65535)->nullable();
			$table->dateTime('send_date')->nullable()->index('idx_send_date');
			$table->dateTime('reserve_send_date')->nullable()->index('idx_reserve_send_date');
			$table->bigInteger('sort_reserve_send_date')->unsigned()->nullable()->index('idx_sort_reserve_send_date');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('melmaga_logs');
	}

}
