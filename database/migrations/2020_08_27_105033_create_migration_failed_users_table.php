<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMigrationFailedUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('migration_failed_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->nullable()->index('idx_client_id');
			$table->integer('login_id')->nullable();
			$table->string('email', 254)->nullable()->index('idx_email');
			$table->boolean('is_quit')->nullable();
			$table->boolean('status')->nullable();
			$table->boolean('disable')->nullable();
			$table->dateTime('reg_date')->nullable();
			$table->dateTime('last_access_date')->nullable();
			$table->text('description', 65535)->nullable();
			$table->text('memo', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('migration_failed_users');
	}

}
