<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTopProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('top_products', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('tipster')->index('idx_tipster');
			$table->integer('quantity')->unsigned()->default(30);
			$table->string('title')->index('title');
			$table->text('comment', 65535)->nullable();
			$table->integer('open_flg')->default(0)->index('idx_open_flg');
			$table->string('groups')->nullable()->index('idx_group');
			$table->text('saddle', 65535)->nullable();
			$table->text('tickets', 65535)->nullable();
			$table->integer('order_num')->default(1)->index('idx_order');
			$table->integer('money')->default(0)->index('idx_money');
			$table->integer('point')->default(0);
			$table->text('discount', 65535)->nullable();
			$table->dateTime('start_date')->nullable()->index('idx_start_date');
			$table->bigInteger('sort_start_date')->nullable()->index('idx_sort_start_date');
			$table->dateTime('end_date')->nullable()->index('idx_end_date');
			$table->bigInteger('sort_end_date')->nullable()->index('idx_sort_end_date');
			$table->date('sort_date')->nullable()->index('idx_sort_date');
			$table->bigInteger('sort_sold_out_date')->nullable()->index('idx_sort_sold_out');
			$table->dateTime('sold_out_date')->nullable()->index('idx_sold_out');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('top_products');
	}

}
