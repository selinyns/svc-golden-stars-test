<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettlementTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('settlement_types', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->string('name');
			$table->integer('active')->default(0)->index('idx_active');
			$table->string('clientip', 20);
			$table->string('netbank_clientip', 20)->nullable();
			$table->integer('sendid_length');
			$table->string('speed_credit_url');
			$table->string('credit_url');
			$table->string('netbank_url');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settlement_types');
	}

}
