<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePointSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('point_settings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->index('idx_category_id');
			$table->integer('money');
			$table->integer('point');
			$table->string('disp_msg')->nullable();
			$table->string('remarks')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('point_settings');
	}

}
