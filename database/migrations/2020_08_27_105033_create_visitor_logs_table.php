<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVisitorLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('visitor_logs', function(Blueprint $table)
		{
			$table->integer('forecast_id')->index('idx_forecast_id');
			$table->integer('client_id');
			$table->integer('category');
			$table->timestamps();
			$table->unique(['forecast_id','client_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visitor_logs');
	}

}
