<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccessLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('access_logs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('login_id')->index('idx_login_id');
			$table->boolean('pay_type')->index('idx_pay_type');
			$table->date('login_date')->index('idx_login_date');
			$table->timestamps();
			$table->unique(['login_id','login_date']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('access_logs');
	}

}
