<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateYearResultAccessLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('year_result_access_logs', function(Blueprint $table)
		{
			$table->integer('access_date')->unique();
			$table->integer('no_pay');
			$table->integer('pay');
			$table->integer('total');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('year_result_access_logs');
	}

}
