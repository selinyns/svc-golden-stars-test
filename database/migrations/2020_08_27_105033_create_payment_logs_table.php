<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaymentLogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::connection('gsta')->create('payment_logs', function(Blueprint $table)
		{
			$table->increments('payment_id');
			$table->integer('agency_id')->unsigned()->index('idx_agency_id');
			$table->boolean('pay_type')->index('idx_pay_type');
			$table->string('login_id')->index('idx_login_id');
			$table->integer('type');
			$table->integer('product_id')->index('idx_product_id');
			$table->integer('order_id')->nullable()->index('idx_order_id');
			$table->integer('money')->index('idx_money');
			$table->integer('point')->unsigned()->default(0)->index('idx_point');
			$table->string('ad_cd')->nullable()->index('idx_ad_cd');
			$table->string('status', 20)->nullable()->index('idx_status');
			$table->string('sendid', 40)->nullable()->index('idx_sendid');
			$table->string('tel', 11)->nullable();
			$table->string('email', 50)->nullable();
			$table->string('username', 50)->nullable();
			$table->string('cont', 3)->nullable();
			$table->dateTime('regist_date')->nullable()->index('idx_regist_date');
			$table->integer('pay_count')->index('idx_pay_count');
			$table->integer('sort_date')->nullable()->index('idx_sort_date');
			$table->timestamps();
			$table->unique(['product_id','order_id','type']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payment_logs');
	}

}
