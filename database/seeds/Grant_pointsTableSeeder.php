<?php

use Illuminate\Database\Seeder;

class Grant_pointsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('gsta')->table('grant_points')->insert([
            'type' => 'loginbonus',
            'point' => 1,
            'dispmsg' => '<div id="loginbonus" class="white-popup">
 <p class="test"><b><font size="5">ログインボーナス</font></b></p>
 <p>ログインボーナスポイントをゲットしました！<br>
 明日ログインするとまた無料でポイントをゲットできます！
 </p>
 <style>
 p.test{color:#FF0000;}
 </style>
 </div>',
            'disptime' => 5,
            'disp_flg' => 1,
            'created_at' => date("Y/m/d h:m:s"),
            'updated_at' => date("Y/m/d h:m:s"),
        ]);

        DB::connection('gsta')->table('grant_points')->insert([
            'type' => 'registed',
            'point' => 100,
            'dispmsg' => null,
            'disptime' => null,
            'disp_flg' => 0,
            'created_at' => date("Y/m/d h:m:s"),
            'updated_at' => date("Y/m/d h:m:s"),
        ]);
    }
}
