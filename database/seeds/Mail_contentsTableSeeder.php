<?php

use Illuminate\Database\Seeder;

class Mail_contentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '仮登録完了リメール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '仮登録完了のご連絡',
				'body' => '◆━━━━━━━━━━━━━━━━:
 -%site_name-仮登録が完了しました
 ━━━━━━━━━━━━━━━━◆
 
 この度は-%site_name-へ申し込み頂き、誠にありがとうございます。
 
 [注意]当サイトのご利用は競馬の情報を取り扱うため20歳以上の方のみご利用頂けます。
 
 20歳以上であれば、ご確認の上、下記のURLをクリックし、本登録を完了して下さい。
 
 -%regist_url-/-%token-
 
 ※サイトからのメール配信に同意の上、ご登録ください。
 ※このメールに心当たりのない方は、お手数ですが本メールを削除してください。
 
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => 'ユーザー登録完了メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '【本登録】が完了致しました!',
				'body' => 'GOLDEN★STARSへようこそ
 
 この度はご登録頂きまして誠にありがとうございます。
 
 【本登録】が完了致しましたので、会員様の会員ID、PASSは以下となります。
 ----------------------------
  　会員ID:-%login_id-
    PASS:-%password-
 ----------------------------
 ※このメールは会員ID・パスワードが記載されておりますので、大切に保管して下さい。
 
 ▼簡単ログインはこちら▼
 -%simple_login-/-%token-
 
 ※このメールに心当たりのない方は、お手数ですが本メールを削除してください。
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '登録済みメール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '[-%site_name-]です！',
				'body' => 'お問い合わせ誠にありがとうございます！
 
 このメールアドレスは既に登録されております。
 
 ID:-%login_id-
 PASS:-%password-
 
 ログインページ
 -%top_url-
 
 ▼簡単ログインはこちら▼
 -%simple_login-/-%token-
 
 ※このメールに心当たりのない方は、お手数ですが本メールを削除してください。
 
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => 'パス忘れリメール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '-%site_name-です。',
				'body' => 'お問い合わせ誠にありがとうございます。
 
 お客様のIDとパスワードをお知らせ致します。
 
 ID:-%login_id-
 PASS:-%password-
 
 ※このメールは会員ID・パスワードが記載されておりますので、大切に保管して下さい。
 
 ▼こちらから今すぐログインできます▼
 -%top_url-
 
 ▼簡単ログインはこちら▼
 -%simple_login-/-%token-
 
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '銀行振込決済完了メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '銀行振込決済完了メール',
				'body' => 'この度は[-%site_name-]をご利用頂き、誠にありがとうございます。
 
 [銀行振込決済]が完了致しました。
 
 
 ■これからの流れ
 ￣￣￣￣￣￣￣￣￣￣￣￣￣￣
 ご提供レースの情報公開は
 【レース前日18：00以降】
 または
 【レース当日11:30～13：00】
 
 ※レース前日の20：00までに情報が公開されていない場合は、
 レース当日11：30～13：00の公開になります。
 
 ※万が一、レース当日の13：00までに情報が公開にならない場合は、お手数ですがご連絡ください。
 
 ※情報公開後はメールでも公開のご連絡をいたします。 
 
 【ご注意】
 -------------------
 
 ここ最近、情報公開メールが迷惑メール等と誤認され不着の場合がございます。
 
 万が一、そのような理由で情報公開のメールが届かなくとも、「情報公開」ページには情報は公開されております。
 
 誠に恐れ入りますが、情報公開時間になりましたら、必ず会員様ご自身でログインして頂き、「情報公開」ページをご確認ください。
 
 「情報公開のメールが届かなかったから情報を見ることができなかった」というお問い合わせにつきましてはご対応できませんので、ご了承ください。
 
 また弊社ホームページ上にあります「情報公開ページ」内にて[買い目情報]を公開させて頂きますのでくれぐれもお見逃しのないようご確認頂き、どうぞご期待下さりますよう宜しくお願い致します。(また弊社では独自の情報網を構築しており、情報漏洩の都合上、基本的な例外を除き、【買い目のメール配信】は行っておりませんので何卒、ご了承下さりますよう宜しくお願い致します。)
 
 ━━━━━━━━━━━━━━━━━
 　　　　　-%site_name-
 ━━━━━━━━━━━━━━━━━
 
 ■ログインはコチラ▼
 -%simple_login-/-%token-
 
 ▼ご登録情報▼
 会員ID:-%login_id-
 パスワード:-%password-
 
 【お問い合わせ 配信停止】
 　　-%info_mail-
 ※18：30以降のメールご対応は翌日になる場合があります。
 
 ◇◆◇◆◇◆◇◆◇◆◇◇◆◇◆◇◆◇◆◇◆◇
 Copyright(C)GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => 'クレジット決済完了メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '[-%site_name-]決済完了のお知らせ',
				'body' => 'この度は[-%site_name-]をご利用頂き、誠にありがとうございます。
 
 [クレジット決済]が完了致しました。
 
 ■これからの流れ
 ￣￣￣￣￣￣￣￣￣￣￣￣￣￣
 ご提供レースの情報公開は
 【レース前日18：00以降】
 または
 【レース当日11:30～13：00】
 
 ※レース前日の20：00までに情報が公開されていない場合は、
 レース当日11：30～13：00の公開になります。
 
 ※万が一、レース当日の13：00までに情報が公開にならない場合は、お手数ですがご連絡ください。
 
 ※情報公開後はメールでも公開のご連絡をいたします。 
 
 【ご注意】
 -------------------
 
 ここ最近、情報公開メールが迷惑メール等と誤認され不着の場合がございます。
 
 万が一、そのような理由で情報公開のメールが届かなくとも、「情報公開」ページには情報は公開されております。
 
 誠に恐れ入りますが、情報公開時間になりましたら、必ず会員様ご自身でログインして頂き、「情報公開」ページをご確認ください。
 
 「情報公開のメールが届かなかったから情報を見ることができなかった」というお問い合わせにつきましてはご対応できませんので、ご了承ください。
 
 また弊社ホームページ上にあります「情報公開ページ」内にて[買い目情報]を公開させて頂きますのでくれぐれもお見逃しのないようご確認頂き、どうぞご期待下さりますよう宜しくお願い致します。(また弊社では独自の情報網を構築しており、情報漏洩の都合上、基本的な例外を除き、【買い目のメール配信】は行っておりませんので何卒、ご了承下さりますよう宜しくお願い致します。)
 
 ━━━━━━━━━━━━━━━━━
 　　　　　-%site_name-
 ━━━━━━━━━━━━━━━━━
 
 ■ログインはコチラ▼
 -%simple_login-/-%token-
 
 ▼ご登録情報▼
 会員ID:-%login_id-
 パスワード:-%password-
 
 【お問い合わせ 配信停止】
 　　-%info_mail-
 ※18：30以降のメールご対応は翌日になる場合があります。
 
 ◇◆◇◆◇◆◇◆◇◆◇◇◆◇◆◇◆◇◆◇◆◇
 Copyright(C)GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => 'ネットバンク決済完了メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '[-%site_name-]決済完了のお知らせ',
				'body' => 'この度は[-%site_name-]をご利用頂き、誠にありがとうございます。
 
 [ネットバンク決済]が完了致しました。
 
 ■これからの流れ
 ￣￣￣￣￣￣￣￣￣￣￣￣￣￣
 ご提供レースの情報公開は
 【レース前日18：00以降】
 または
 【レース当日11:30～13：00】
 
 ※レース前日の20：00までに情報が公開されていない場合は、
 レース当日11：30～13：00の公開になります。
 
 ※万が一、レース当日の13：00までに情報が公開にならない場合は、お手数ですがご連絡ください。
 
 ※情報公開後はメールでも公開のご連絡をいたします。 
 
 【ご注意】
 -------------------
 
 ここ最近、情報公開メールが迷惑メール等と誤認され不着の場合がございます。
 
 万が一、そのような理由で情報公開のメールが届かなくとも、「情報公開」ページには情報は公開されております。
 
 誠に恐れ入りますが、情報公開時間になりましたら、必ず会員様ご自身でログインして頂き、「情報公開」ページをご確認ください。
 
 「情報公開のメールが届かなかったから情報を見ることができなかった」というお問い合わせにつきましてはご対応できませんので、ご了承ください。
 
 また弊社ホームページ上にあります「情報公開ページ」内にて[買い目情報]を公開させて頂きますのでくれぐれもお見逃しのないようご確認頂き、どうぞご期待下さりますよう宜しくお願い致します。(また弊社では独自の情報網を構築しており、情報漏洩の都合上、基本的な例外を除き、【買い目のメール配信】は行っておりませんので何卒、ご了承下さりますよう宜しくお願い致します。)
 
 ━━━━━━━━━━━━━━━━━
 　　　　　-%site_name-
 ━━━━━━━━━━━━━━━━━
 
 ■ログインはコチラ▼
 -%simple_login-/-%token-
 
 ▼ご登録情報▼
 会員ID:-%login_id-
 パスワード:-%password-
 
 【お問い合わせ 配信停止】
 　　-%info_mail-
 ※18：30以降のメールご対応は翌日になる場合があります。
 
 ◇◆◇◆◇◆◇◆◇◆◇◇◆◇◆◇◆◇◆◇◆◇
 Copyright(C)GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => 'メールアドレス変更メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => 'メールアドレス変更確認',
				'body' => '＝＝＝＝＝＝＝＝＝＝
 メールアドレスの変更を完了させるには
 下記のURLをクリックしてください。
 
 -%mail_chg_url-
 
 
 ※このメールに心当たりのない方は、お手数ですが本メールを削除してください。
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '仮登録完了リメール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '仮登録完了のご連絡',
				'body' => '◆━━━━━━━━━━━━━━━━:
 -%site_name-仮登録が完了しました
 ━━━━━━━━━━━━━━━━◆
 
 この度は-%site_name-へ申し込み頂き、誠にありがとうございます。
 
 [注意]当サイトのご利用は競馬の情報を取り扱うため20歳以上の方のみご利用頂けます。
 
 20歳以上であれば、ご確認の上、下記のURLをクリックし、本登録を完了して下さい。
 
 -%regist_url-/-%token-
 
 ※サイトからのメール配信に同意の上、ご登録ください。
 ※このメールに心当たりのない方は、お手数ですが本メールを削除してください。
 
 _/_/_/_/_/_/_/_/_/
 ■お問合せ／配信停止■
 -%company_navidial-
 -%info_mail-
 _/_/_/_/_/_/_/_/_/
 Copyright(C) GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '銀行振込注文受付メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '銀行振込注文受付メール',
				'body' => 'この度は[-%site_name-]をご利用頂き、誠にありがとうございます。
 
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 銀行振込決済 注文受付完了メール
 宛先：-%usermail-
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 このメールは、お申し込み頂きましたお振込情報などが記載されておりますの
 で、大切に保管頂きますようお願い申し上げます。
 
 ■ お振込先口座名称：三井住友銀行
 ■支店名：世田谷支店
 ■口座番号：普通 5566552
 ■振込先名義：有限会社ルーツ
 ■ お振込金額      ：-%transfer_amount-円
 ■お振込み番号：-%login_id-
 ■ 注文処理日時    ：-%order_date-
 ■ サイト連絡先    ：info@g-stars.net
 ■ オーダNo        ：-%order_id-
 
 ※ご入金の際、振込人欄に、お名前ではなく【お振込番号　-%login_id-】をご入力の上、
 電信扱いにてお振り込みください。
 
 ━━━━━━━━━━━━━━━━━
 　　　　　-%site_name-
 ━━━━━━━━━━━━━━━━━
 
 ■ログインはコチラ▼
 -%simple_login-/-%token-
 
 ▼ご登録情報▼
 会員ID:-%login_id-
 パスワード:-%password-
 
 【お問い合わせ 配信停止】
 　　-%info_mail-
 ※18：30以降のメールご対応は翌日になる場合があります。
 
 ◇◆◇◆◇◆◇◆◇◆◇◇◆◇◆◇◆◇◆◇◆◇
 Copyright(C)GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
			DB::connection('gsta')->table('mail_contents')->insert([
				'name' => '銀行振込決済完了メール',
				'from' => 'GOLDEN★STARS',
				'from_mail' => 'info@g-stars.net',
				'subject' => '銀行振込決済完了メール',
				'body' => 'この度は[-%site_name-]をご利用頂き、誠にありがとうございます。
 
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 銀行振込決済 完了メール
 宛先：-%usermail-
 ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
 このメールは、お申し込み頂きましたお振込情報などが記載されておりますの
 で、大切に保管頂きますようお願い申し上げます。
 
 ■ お振込先口座名称：三井住友銀行
 ■支店名：世田谷支店
 ■口座番号：普通 5566552
 ■振込先名義：有限会社ルーツ
 ■ お振込金額      ：-%transfer_amount-円
 ■ 注文処理日時    ：-%order_date-
 ■ サイト連絡先    ：info@g-stars.net
 ■ オーダNo        ：-%order_id-
 
 ━━━━━━━━━━━━━━━━━
 　　　　　-%site_name-
 ━━━━━━━━━━━━━━━━━
 
 ■ログインはコチラ▼
 -%simple_login-/-%token-
 
 ▼ご登録情報▼
 会員ID:-%login_id-
 パスワード:-%password-
 
 【お問い合わせ 配信停止】
 　　-%info_mail-
 ※18：30以降のメールご対応は翌日になる場合があります。
 
 ◇◆◇◆◇◆◇◆◇◆◇◇◆◇◆◇◆◇◆◇◆◇
 Copyright(C)GOLDEN★STARS All Right Reserved.',
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s"),
			]);
    }
}
