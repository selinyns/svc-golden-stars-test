<?php

use Illuminate\Database\Seeder;

class TopContentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        DB::connection('gsta')->table('top_contents')->insert(array (
            0 => 
            array (
                'title' => '2つ星ポイント情報【TOPページ用】',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-04-08 11:34:00',
                'sort_start_date' => 20200408113400,
                'end_date' => '3020-12-02 11:34:00',
                'sort_end_date' => 30201202113400,
                'url' => NULL,
                'img' => '8.png',
                'html_body' => '2つ星ポイント情報',
                'sort_date' => '2020-04-08',
                'created_at' => '2020-05-01 02:34:45',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            1 => 
            array (
                'title' => 'TOPコンテンツ商品B',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-04-07 11:35:00',
                'sort_start_date' => 20200407113500,
                'end_date' => '2020-08-18 11:35:00',
                'sort_end_date' => 20200818113500,
                'url' => NULL,
                'img' => '9.png',
                'html_body' => '商品B',
                'sort_date' => '2020-04-07',
                'created_at' => '2020-05-01 02:35:25',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            2 => 
            array (
                'title' => 'TOPコンテンツの商品C',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-11-18 11:35:00',
                'sort_end_date' => 20201118113500,
                'url' => NULL,
                'img' => '10.png',
                'html_body' => '商品C',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:36:13',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            3 => 
            array (
                'title' => 'TOPコンテンツの商品D',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-09-17 11:36:00',
                'sort_end_date' => 20200917113600,
                'url' => NULL,
                'img' => '11.png',
                'html_body' => '商品D',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:36:36',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            4 => 
            array (
                'title' => 'TOPコンテンツの商品E',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-11-11 11:36:00',
                'sort_end_date' => 20201111113600,
                'url' => NULL,
                'img' => '12.png',
                'html_body' => '商品E',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:36:59',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            5 => 
            array (
                'title' => 'TOPコンテンツの商品F',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-12-15 11:37:00',
                'sort_end_date' => 20201215113700,
                'url' => NULL,
                'img' => '13.png',
                'html_body' => '商品F',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:37:22',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            6 => 
            array (
                'title' => 'TOPコンテンツの商品G',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-12-01 11:37:00',
                'sort_end_date' => 20201201113700,
                'url' => NULL,
                'img' => '14.png',
                'html_body' => '商品G',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:37:50',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            7 => 
            array (
                'title' => 'TOPコンテンツのレギュラーA',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-10-21 11:38:00',
                'sort_end_date' => 20201021113800,
                'url' => NULL,
                'img' => '15.png',
                'html_body' => 'レギュラーA',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:38:17',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            8 => 
            array (
                'title' => 'TOPコンテンツのレギュラーB',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-09-23 11:38:00',
                'sort_end_date' => 20200923113800,
                'url' => NULL,
                'img' => '16.png',
                'html_body' => 'レギュラーB',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 02:38:45',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            9 => 
            array (
                'title' => 'TOPコンテンツのレギュラーC',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-10-14 12:11:00',
                'sort_end_date' => 20201014121100,
                'url' => NULL,
                'img' => '17.png',
                'html_body' => 'TOPコンテンツのレギュラーCです',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 03:11:34',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            10 => 
            array (
                'title' => 'TOPコンテンツの商品H',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-10-13 23:59:00',
                'sort_end_date' => 20201013235900,
                'url' => NULL,
                'img' => '18.png',
                'html_body' => 'TOPコンテンツの商品H',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-01 14:59:17',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            11 => 
            array (
                'title' => 'ポイント情報',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-09 15:00:00',
                'sort_start_date' => 20200509150000,
                'end_date' => '2020-05-30 20:00:00',
                'sort_end_date' => 20200530200000,
                'url' => NULL,
                'img' => '19.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-09',
                'created_at' => '2020-05-09 06:43:42',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            12 => 
            array (
                'title' => '重賞有力情報',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-09 17:00:00',
                'sort_start_date' => 20200509170000,
                'end_date' => '2020-05-30 17:55:00',
                'sort_end_date' => 20200530175500,
                'url' => NULL,
                'img' => '21.png',
                'html_body' => 'NHKマイルカップ重賞
◎アノウマ
〇コノウマ
▲ドノウマ',
                'sort_date' => '2020-05-09',
                'created_at' => '2020-05-09 08:55:19',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            13 => 
            array (
            'title' => '5/10(日)真田',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => '6',
                'order_num' => 1,
                'start_date' => '2019-06-07 20:00:00',
                'sort_start_date' => 20190607200000,
                'end_date' => '2020-06-06 19:21:00',
                'sort_end_date' => 20200606192100,
                'url' => NULL,
                'img' => '22.png',
                'html_body' => '私は真田だ！',
                'sort_date' => '2019-06-07',
                'created_at' => '2020-05-10 02:55:24',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            14 => 
            array (
            'title' => '5/17(土)幸田',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-10 13:37:00',
                'sort_start_date' => 20200510133700,
                'end_date' => '2020-05-23 13:37:00',
                'sort_end_date' => 20200523133700,
                'url' => NULL,
                'img' => NULL,
                'html_body' => NULL,
                'sort_date' => '2020-05-10',
                'created_at' => '2020-05-10 04:37:23',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            15 => 
            array (
                'title' => 'hosino',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-01 12:35:00',
                'sort_start_date' => 20200501123500,
                'end_date' => '2020-05-23 12:35:00',
                'sort_end_date' => 20200523123500,
                'url' => NULL,
                'img' => '25.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-01',
                'created_at' => '2020-05-11 03:34:46',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            16 => 
            array (
                'title' => 'TOPコンテンツのレギュラーD',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-03-01 00:00:00',
                'sort_start_date' => 20200301000000,
                'end_date' => '2020-12-23 11:17:00',
                'sort_end_date' => 20201223111700,
                'url' => NULL,
                'img' => NULL,
                'html_body' => 'TOPコンテンツのレギュラーD',
                'sort_date' => '2020-03-01',
                'created_at' => '2020-05-12 02:18:03',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            17 => 
            array (
                'title' => 'TOPコンテンツのレギュラーE',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-04-26 11:19:00',
                'sort_start_date' => 20200426111900,
                'end_date' => '2020-11-18 11:19:00',
                'sort_end_date' => 20201118111900,
                'url' => NULL,
                'img' => NULL,
                'html_body' => 'TOPコンテンツのレギュラーE',
                'sort_date' => '2020-04-26',
                'created_at' => '2020-05-12 02:19:18',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            18 => 
            array (
                'title' => '重賞有力情報',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-12 11:00:00',
                'sort_start_date' => 20200512110000,
                'end_date' => '2020-05-23 19:21:00',
                'sort_end_date' => 20200523192100,
                'url' => NULL,
                'img' => '28.png',
                'html_body' => '<style>
/*---style---*/
.regular_detail{width:100%;margin:0 auto;background:#FFF;color:#333;font-family: Verdana, Roboto, "Droid Sans", "游ゴシック", YuGothic, "メイリオ", Meiryo, "ヒラギノ角ゴ ProN W3", "Hiragino Kaku Gothic ProN", "ＭＳ Ｐゴシック", sans-serif !important;font-size: initial;
word-break: break-all;}
.regular_detail img{max-width:100%;}
.regular_detail .ttl01{background: #be9031;color: #fff;font-size:25px;font-weight: bold;text-align: center;padding: 5px;margin-bottom: 15px;line-height: 1.4;}
.regular_detail .ttl02{color: #568302;font-size: 20px;font-weight: bold;padding: 10px;margin: 10px;border: 3px solid #568302;line-height: 1.4;}
.regular_detail .text_contents{padding:15px 10px;font-size: 16px;}
.regular_detail .text_contents *{line-height: 1.6;font-size:100%;}
.regular_detail .text_contents > p{margin-bottom:20px;}
</style>

<div class="regular_detail">
<!------------ ※※※ ↓編集ここから↓ ※※※-------------------->
<a href="contents.php?id=17173"><img src="http://premium-h.jp/image/display_image/bnr_mailset.png" width="100%"></a>


<!--※ ↓タイトル↓ ※-->
<div class="ttl01">
5月17日(日)<br>
ヴィクトリアマイル(G1)<br>
</div>
<div class="ttl02">
◎アーモンドアイ<br>
〇サウンドキアラ<br>
▲ラヴズオンリーユー<br>
</div>

<!--※ ↓本文 --><div class="text_contents"><!-- 本文↓ ※-->


<p><b>【◎アーモンドアイ】</b><br>
目玉となるのはアーモンドアイ（牝5、美浦・国枝栄厩舎）。<br>
単勝オッズ1.5倍の支持を受けながらも9着と敗れた昨年の有馬記念（G1）からの巻き返しを期す。<br>
今年はドバイターフ（G1）からの始動を予定していたが、新型コロナウイルス感染拡大防止の影響で中止となり、ヴィクトリアマイルが復帰初戦となった。<br>
天皇賞・秋（G1）を圧勝して臨んだ有馬記念の敗戦は、1週目のスタンド前で引っ掛かってしまい、ガス欠となったことが敗因ともいわれているが、香港C（G1）を熱発で回避した影響も少なからずあったのではないだろうか。<br>
順調さを欠いた前走に比して今回の調整過程は順調だ。<br>
国枝師も「体調面で気にするところはなく、普通にこられています」と不安はない。<br>女王復権の準備は着々と整っている。<br>
<br>

<p><b>【〇:サウンドキアラ】</b><br>
下剋上を狙っているのがサウンドキアラ（牝5、栗東・安達昭夫厩舎）。<br>
昨年のヴィクトリアマイルでは7着に敗れたが、この1年で着実に力をつけた。<br>
今年に入って京都金杯（G3）、京都牝馬S（G3）、阪神牝馬S（G2）と重賞3連勝を達成。<br>
4月26日の落馬負傷で心配された松山弘平騎手も今週から復帰。<br>
人馬ともに今年ブレイクしたコンビで女王打倒を目指す。<br>
<br>

<p><b>【▲:ラヴズオンリーユー】</b><br>
昨年のエリザベス女王杯（G1）以来のレースとなるラヴズオンリーユー（牝4、栗東・矢作芳人厩舎）。<br>
前走では自身初の敗戦となる3着に敗れはしたが、クロノジェネシスをオークス（G1）に続いて負かした。<br>
そのクロノジェネシスが大阪杯（G1）を際どい2着に好走したように世代トップクラスの力は証明している。<br>
鋭い末脚を持つ馬だけに、直線の長い東京コースは歓迎だろう。<br>
<br>
<span style="font-size:32px;color:#2c63d7;line-height:1.4em;">
【厳選オススメ有料高配当予想】</span><br>

<span style="font-size:30px;color:#000000;line-height:1.4em;">
◇エボルシオン◇</span><br>
前回実績　123万7350円<br>
<br>
有料高配当予想ご希望の方は<br>
<p><a href="mailto:info@premium-h.jp">こちら</a></p><br>
まで<br>
「有料高配当予想希望」と明記の上、メールを送信して下さい。<br>
後程詳細をお送り致します。',
                'sort_date' => '2020-05-12',
                'created_at' => '2020-05-12 02:42:37',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            19 => 
            array (
                'title' => '19日ジャックバウアー',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2019-06-07 20:00:00',
                'sort_start_date' => 20190607200000,
                'end_date' => '2020-05-30 17:55:00',
                'sort_end_date' => 20200530175500,
                'url' => NULL,
                'img' => NULL,
                'html_body' => NULL,
                'sort_date' => '2019-06-07',
                'created_at' => '2020-05-12 08:55:26',
                'updated_at' => '2020-08-09 02:53:27',
            ),
            20 => 
            array (
                'title' => '5/11真田',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-05 18:05:00',
                'sort_start_date' => 20200505180500,
                'end_date' => '2020-05-26 18:05:00',
                'sort_end_date' => 20200526180500,
                'url' => NULL,
                'img' => NULL,
                'html_body' => NULL,
                'sort_date' => '2020-05-05',
                'created_at' => '2020-05-12 09:05:20',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            21 => 
            array (
            'title' => '5/10(日)幸田',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-12 18:10:00',
                'sort_start_date' => 20200512181000,
                'end_date' => '2020-05-19 18:10:00',
                'sort_end_date' => 20200519181000,
                'url' => NULL,
                'img' => NULL,
                'html_body' => NULL,
                'sort_date' => '2020-05-12',
                'created_at' => '2020-05-12 09:10:29',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            22 => 
            array (
                'title' => '【木曜日更新】追い切り速報',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-15 17:00:00',
                'sort_start_date' => 20200515170000,
                'end_date' => '3020-05-22 18:23:00',
                'sort_end_date' => 30200522182300,
                'url' => NULL,
                'img' => '32.png',
                'html_body' => '<dd>

<h2 class="ttl_01 t_center mb_0">8月16日(日)関屋記念(G3)</h2>
<h2 class="ttl_02 t_center">注目馬「アストラエンブレム」の追い切り速報</h2>
<div class="area_01">
<p>


<br>7歳馬アストラエンブレムは関屋記念と同じ新潟マイル戦の前走・谷川岳Sを快勝。
<br>新潟芝は【3・1・0・1】と相性がいい。5日の1週前追いは坂路で4F50秒8の好時計。
<br>小島師は
<br>「馬なりでこれだけ動けてますからね。去勢した後は2000メートルが持たなくなった半面、1600メートルで安定して走ってくれる。体もしっかりしてきたし、成績が出ている舞台なので」
<br>と悲願の重賞初Vを見据えた。



</p>
</div>

</dd>',
                'sort_date' => '2020-05-15',
                'created_at' => '2020-05-15 09:22:38',
                'updated_at' => '2020-08-13 06:20:09',
            ),
            23 => 
            array (
                'title' => 'ポイント情報【TOPページ用】',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-26 16:00:00',
                'sort_start_date' => 20200526160000,
                'end_date' => '3000-05-30 18:00:00',
                'sort_end_date' => 30000530180000,
                'url' => NULL,
                'img' => '33.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-26',
                'created_at' => '2020-05-26 08:41:00',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            24 => 
            array (
                'title' => '2つ星ポイント情報',
                'type' => 1,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-26 13:00:00',
                'sort_start_date' => 20200526130000,
                'end_date' => '3000-05-30 18:00:00',
                'sort_end_date' => 30000530180000,
                'url' => NULL,
                'img' => '34.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-26',
                'created_at' => '2020-05-26 08:41:39',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            25 => 
            array (
                'title' => '全額返還保証　１００００円分キャッシュバック',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-26 17:00:00',
                'sort_start_date' => 20200526170000,
                'end_date' => '3000-05-30 18:00:00',
                'sort_end_date' => 30000530180000,
                'url' => NULL,
                'img' => '35.png',
                'html_body' => '<div style="text-align:center;">
<span style="background:#980606;font-size:25px;color:#ffffff;display:block;font-weight:bold;"><br />
充実した保証制度<br /><br /></span>
<br />
<span style="background:#363636;font-size:25px;color:#ffffff;display:block;font-weight:bold;"><br />■無期限保証■<br /><br /></span>
<span style="color:#ff00000;font-size:14px">
<br />GOLDEN☆STARSではキャンペーン情報にご購入、ご参加頂いた会員様には充実した保証制度がございます。</span>
<span style="color:#ff00000;font-size:15px"><br />万が一に備え、ご提供させて頂いたキャンペーン情報にて<b>不的中</b>だった場合、
<br />次回以降の央競馬開催に各日1鞍ずつの補填情報をご提供させて頂きます。
<br />補填情報を含めてご利用頂ければ大半の会員様が大幅なプラス収支!!</span>
<br /><span style="color:#c49234;font-size:20px">【!】ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供【!】</span>
<br />
<br />
<span style="background:#363636;font-size:23px;color:#ffffff;display:block;font-weight:bold;"><br />■新規登録会員様には10,000円分のポイントプレゼント!!■<br /><br /></span>
<br /><span style="color:#ff00000;font-size:15px">本登録後、すぐに使えるキャンペーンでの割引購入やサイト内にあるポイント情報閲覧に必要な
<br /><span style="color:#c49234;font-size:20px;b">ポイントを10,000円分(100pt)必ずプレゼント!!<br /></span>
<br />

<span style="background:#980606;font-size:25px;color:#ffffff;display:block;font-weight:bold;"><br />
Ｑ＆Ａ<br /><br /></span>
<br />
<hr>
<span style="color:#01206c;font-size:19px">【Ｑ】無期限保証制度は1回限りの提供なのでしょうか？</span>
<br />
<br /><span style="color:#C49234;font-size:19px">【Ａ】会員様のご利用状況によりますが、
<br>ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供させて頂きます。</span>
<hr>
<br>
<hr>
<span style="color:#01206c;font-size:19px">【Ｑ】ポイントは有効期限がありますか？</span>
<br />
<br /><span style="color:#C49234;font-size:19px">【Ａ】基本的には弊社サービスが終了するまでご使用頂けます。</span>
<hr>
<br>
<hr>
<span style="color:#01206c;font-size:19px">【Ｑ】登録したら請求がされるのでしょうか？</span>
<br />
<br /><span style="color:#C49234;font-size:19px">【Ａ】登録は無料です。キャンペーンご購入の際は有料となりますが、
<br>サイト内にある無料コンテンツにつきましては完全無料でご覧いただけます</span>
<hr>',
                'sort_date' => '2020-05-26',
                'created_at' => '2020-05-26 08:48:57',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            26 => 
            array (
                'title' => '【火曜日更新】重賞有力情報',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-06-30 16:00:00',
                'sort_start_date' => 20200630160000,
                'end_date' => '3000-07-06 16:00:00',
                'sort_end_date' => 30000706160000,
                'url' => NULL,
                'img' => '37.png',
                'html_body' => '<dd>

<h2 class="ttl_01 t_center mb_S">8月16日(日)関屋記念(G3)</h2>
<dl class="box_02 t_center">
<dt>推奨馬</dt>
<dd class="f_L f_bold">
◎プリモシーン<br>
〇メイケイダイハード<br>
▲エントシャイデン
</dd>
</dl>

<h3 class="ttl_02 bl mb_0">◎プリモシーン</h3>
<div class="area_01 mb_L">


<br>前走前々走では心と体のバランスが取れず、本来のパフォーマンスを発揮できなかった。木村師も「ここ２戦は失敗しました。ピークに持っていくのが難しいタイプ」と分析する。
<br>中間の調教では調整法に変化をつけ、軽めのメニューを多くこなして体をつくる。
<br>１週前追い切りはハードにＧ１馬ステルヴィオをパートナーに指名して、長めから意欲的に併せ馬を敢行した。
<br>
<br>課題であった心とバランスも中間の調整法により、克服の方向へ進んでいる

</div>

<h3 class="ttl_02 bl mb_0">〇メイケイダイハード</h3>
<div class="area_01 mb_L">


<br>関屋記念へ向けて６日の１週前追い切りは栗東・坂路を馬なりで５２秒６―１２秒９。という好調を見せる一面も。
<br>中竹調教師曰く
<br>「使っても大きく変わることがなく、本当に安定している。引き続きいい状態です。前走も暑かったので、レース前に精神的に大丈夫かと心配していましたが、あの結果ですからね」と暑い夏場でも好走できる一言も。
<br>
<br>中竹厩舎は夏に強い。重賞２３勝のうち７、８月で７勝を挙げる。穴馬の候補馬として一考の価値はありそうだ。
</div>

<h3 class="ttl_02 bl mb_0">▲エントシャイデン</h3>
<div class="area_01 mb_L">

<br>前走の中京記念が大外枠から直線よく伸びて3着。
<br>広岡助手は「差しが利く展開になったのもあるけど、いい脚を使ってくれました」と振り返る。
<br>「つかみどころがないタイプだけど、この中間も変わらず順調。馬場が荒れていた最終週の阪神から新潟に替わるのは、いいと思いますよ」と期待を口にしていた

</div>


</dd>',
                'sort_date' => '2020-06-30',
                'created_at' => '2020-05-26 08:52:09',
                'updated_at' => '2020-08-11 06:00:39',
            ),
            27 => 
            array (
                'title' => '全額返還保証・10000円分キャッシュバック',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-27 11:00:00',
                'sort_start_date' => 20200527110000,
                'end_date' => '3000-05-27 11:00:00',
                'sort_end_date' => 30000527110000,
                'url' => NULL,
                'img' => '38.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-27',
                'created_at' => '2020-05-27 02:53:24',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            28 => 
            array (
                'title' => '全額返還保証・10000円分キャッシュバック！',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-27 11:55:00',
                'sort_start_date' => 20200527115500,
                'end_date' => '3000-05-27 11:55:00',
                'sort_end_date' => 30000527115500,
                'url' => NULL,
                'img' => '39.png',
                'html_body' => '<dd>
<h2 class="ttl_01 mb_S t_center">8/9(日)～8/16(日)限定保証</h2>

<div class="area_01 mb_LL">
<p>8/9(日)～8/16(日)にキャンペーン情報にご参加頂いた会員様の中で、万が一不的中だった場合は、次回弊社キャンペーン情報を<span class="f_red f_bold">半額</span>でご案内致します。
</div>

<h2 class="ttl_01 mb_S t_center">充実した保証制度</h2>
<h3 class="ttl_02 bl">■ 無期限保証</h3>
<div class="area_01">
<p>GOLDEN☆STARSではキャンペーン情報にご購入、ご参加頂いた会員様には<br>
充実した<span class="f_red f_bold">保証制度</span>がございます。<br><br>
万が一に備え、ご提供させて頂いたキャンペーン情報にて<b>不的中</b>だった場合、<br>
次回以降の央競馬開催に各日1鞍ずつの補填情報をご提供させて頂きます。<br>
<br>
補填情報を含めてご利用頂ければ<span class="f_red f_bold">大半の会員様が大幅なプラス収支!!</span>
</p>
<div class="box_01"><p class="f_bold">【!】ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供【!】</p></div>
</div>

<h3 class="ttl_02 bl">■ 新規登録会員様には10,000円分のポイントプレゼント!!</h3>
<div class="area_01 mb_LL">
<p>本登録後、すぐに使えるキャンペーンでの割引購入やサイト内にあるポイント情報閲覧に必要なポイントを</p>
<p class="f_red f_bold">10,000円分(100pt)必ずプレゼント!!</p>
</div>


<h2 class="ttl_01 mb_S t_center">Ｑ＆Ａ</h2>
<dl class="box_02">
<dt>【Ｑ】無期限保証制度は1回限りの提供なのでしょうか？</dt>
<dd>【Ａ】会員様のご利用状況によりますが、<br>
<span class="f_red f_bold">ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供</span>させて頂きます。</dd>
<dt>【Ｑ】ポイントは有効期限がありますか？</dt>
<dd>【Ａ】基本的には弊社サービスが終了するまでご使用頂けます。</dd>
<dt>【Ｑ】登録したら請求がされるのでしょうか？</dt>
<dd>【Ａ】登録は無料です。キャンペーンご購入の際は有料となりますが、<br>
サイト内にある無料コンテンツにつきましては完全無料でご覧いただけます</dd>
</dl>





</dd>',
                'sort_date' => '2020-05-27',
                'created_at' => '2020-05-27 02:54:58',
                'updated_at' => '2020-08-09 09:38:51',
            ),
            29 => 
            array (
                'title' => '無料情報',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-29 10:12:00',
                'sort_start_date' => 20200529101200,
                'end_date' => '3020-05-29 10:12:00',
                'sort_end_date' => 30200529101200,
                'url' => NULL,
                'img' => '40.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-29',
                'created_at' => '2020-05-29 01:11:46',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            30 => 
            array (
                'title' => 'ポイント情報',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-29 11:00:00',
                'sort_start_date' => 20200529110000,
                'end_date' => '3000-05-29 11:00:00',
                'sort_end_date' => 30000529110000,
                'url' => NULL,
                'img' => '41.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-29',
                'created_at' => '2020-05-29 03:32:31',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            31 => 
            array (
                'title' => '2つ星ポイント情報',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-29 12:00:00',
                'sort_start_date' => 20200529120000,
                'end_date' => '3000-05-29 12:00:00',
                'sort_end_date' => 30000529120000,
                'url' => NULL,
                'img' => '42.png',
                'html_body' => NULL,
                'sort_date' => '2020-05-29',
                'created_at' => '2020-05-29 03:33:16',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            32 => 
            array (
                'title' => 'これがさいしんのえーあいよそうだ！',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => '12',
                'order_num' => 1,
                'start_date' => '2020-06-12 13:54:00',
                'sort_start_date' => 20200612135400,
                'end_date' => '2020-06-12 19:00:00',
                'sort_end_date' => 20200612190000,
                'url' => NULL,
                'img' => '44.gif',
                'html_body' => '<dd>

<h2 class="ttl_01 t_center mb_0">おもしろビデオを募集します</h2>
<h2 class="ttl_02 t_center">あなたが撮ったおもしろビデオを送ってね</h2>
<div class="area_01">
<p>
犬の動画とか<br><br>
<br><img src="/images/upload_images/6.JPG" width="100" height="50" />

<img src="/images/upload_images/7.png">

<img src="https://g-stars.net/images/upload_images/7.png">
</p>
</div>

</dd>',
                'sort_date' => '2020-06-12',
                'created_at' => '2020-06-12 04:54:30',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            33 => 
            array (
                'title' => 'test',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => '8',
                'order_num' => 1,
                'start_date' => '2020-06-12 20:43:00',
                'sort_start_date' => 20200612204300,
                'end_date' => '2020-06-12 20:43:00',
                'sort_end_date' => 20200612204300,
                'url' => NULL,
                'img' => '45.gif',
                'html_body' => '<img src="/images/upload_images/8.png">',
                'sort_date' => '2020-06-12',
                'created_at' => '2020-06-12 11:42:59',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            34 => 
            array (
                'title' => 'testだえす',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-06-13 13:33:00',
                'sort_start_date' => 20200613133300,
                'end_date' => '2020-06-13 17:00:00',
                'sort_end_date' => 20200613170000,
                'url' => NULL,
                'img' => '46.png',
                'html_body' => '<img src="https://g-stars.net/images/upload_images/8.png">',
                'sort_date' => '2020-06-13',
                'created_at' => '2020-06-13 04:33:14',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            35 => 
            array (
                'title' => '※上書き禁止※【木曜日更新】追い切り速報htmlフォーマット',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-06-23 19:17:00',
                'sort_start_date' => 20200623191700,
                'end_date' => '3020-06-23 19:17:00',
                'sort_end_date' => 30200623191700,
                'url' => NULL,
                'img' => NULL,
                'html_body' => '<dd>

<h2 class="ttl_01 t_center mb_0">〇月〇日(日)〇〇〇(G〇)</h2>
<h2 class="ttl_02 t_center">注目馬「〇〇〇」の追い切り速報</h2>
<div class="area_01">
<p>


<br><br><br><br><br><br>



</p>
</div>

</dd>',
                'sort_date' => '2020-06-23',
                'created_at' => '2020-06-23 10:17:20',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            36 => 
            array (
                'title' => '※上書き禁止※【火曜日更新】重賞有料情報htmlフォーマット',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-06-23 19:19:00',
                'sort_start_date' => 20200623191900,
                'end_date' => '3020-06-23 19:19:00',
                'sort_end_date' => 30200623191900,
                'url' => NULL,
                'img' => NULL,
                'html_body' => '<dd>

<h2 class="ttl_01 t_center mb_S">※月※日(※)※(※1)</h2>
<dl class="box_02 t_center">
<dt>推奨馬</dt>
<dd class="f_L f_bold">
◎※※※※※※<br>
〇※※※※※※<br>
▲※※※※※※
</dd>
</dl>

<h3 class="ttl_02 bl mb_0">◎※※※※※</h3>
<div class="area_01 mb_L">


<br>


</div>

<h3 class="ttl_02 bl mb_0">〇　※※※※※</h3>
<div class="area_01 mb_L">


<br>
<br>


</div>

<h3 class="ttl_02 bl mb_0">▲※※※※※</h3>
<div class="area_01 mb_L">

<br>
<br>


</div>


</dd>',
                'sort_date' => '2020-06-23',
                'created_at' => '2020-06-23 10:19:44',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            37 => 
            array (
                'title' => 'ｳﾞｧｲｵﾚｯﾄ･ﾂﾀﾝｶｰﾒﾝ情報',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-06-30 10:12:00',
                'sort_start_date' => 20200630101200,
                'end_date' => '3020-06-30 10:12:00',
                'sort_end_date' => 30200630101200,
                'url' => NULL,
                'img' => '49.png',
                'html_body' => NULL,
                'sort_date' => '2020-06-30',
                'created_at' => '2020-06-30 01:12:32',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            38 => 
            array (
                'title' => 'えっちなお姉さん情報,,,',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-07-07 16:00:00',
                'sort_start_date' => 20200707160000,
                'end_date' => '2020-07-15 16:29:00',
                'sort_end_date' => 20200715162900,
                'url' => NULL,
                'img' => '50.png',
                'html_body' => '<img src="https://g-stars.net/images/upload_images/12.jpg">
その名は壇蜜
,,,,,,,,',
                'sort_date' => '2020-07-07',
                'created_at' => '2020-07-07 07:28:43',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            39 => 
            array (
                'title' => 'てｓｔ,',
                'type' => 1,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-07-14 13:39:00',
                'sort_start_date' => 20200714133900,
                'end_date' => '2020-07-15 13:39:00',
                'sort_end_date' => 20200715133900,
                'url' => NULL,
                'img' => NULL,
                'html_body' => 'てｓｔ,',
                'sort_date' => '2020-07-14',
                'created_at' => '2020-07-14 04:39:48',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            40 => 
            array (
                'title' => 'アンケート調査',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => '8',
                'order_num' => 1,
                'start_date' => '2020-08-09 09:00:00',
                'sort_start_date' => 20200809090000,
                'end_date' => '2020-08-16 18:00:00',
                'sort_end_date' => 20200816180000,
                'url' => NULL,
                'img' => '52.png',
                'html_body' => '<dd>

<h2 class="ttl_01 mb_S t_center">今週限定！アンケートに答えると1000円分無料でもらえる!!</h2>
<div class="area_01">
<p>今週限定で「好きなキャンペーン情報」をお答え頂ければ<br>
<span class="f_red f_bold">1000円分のポイントをプレゼント</span>致します!<br><br>
キャンペーンに参加した事ある会員様は今まで使った中で最も満足したキャンペーン<br>
参加した事がない会員様は興味があるキャンペーン<br>
でもOK!!<br>
<br>
<span class="f_red f_bold">上位にランクインしたキャンペーンは、次回特別価格にてご案内致します。</span>
</p>
<br>
「お問い合わせ」もしくは「help@g-stars.net」<br>
宛に<br>
<br>
・「好きなキャンペーン情報名」（予想師名）<br>
・「会員様のID」<br>
<br>
を明記の上送信して下さい。<br>
<br>


<h2 class="ttl_01 mb_S t_center">【注意事項】</h2>
<dl class="box_02">
<br><center><dt>・お一人様1回のみ有効</dt><br>
<dt>・ポイントの付与は送信後2週間以内に行います</dt><br>
<dt>・送信完了のメール、ポイント付与完了のメールは送信しない事がございますのでご了承ください。</dt></center></dl>
</dd>',
                'sort_date' => '2020-08-09',
                'created_at' => '2020-08-08 03:44:40',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            41 => 
            array (
                'title' => 'アンケート調査',
                'type' => 2,
                'open_flg' => 1,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-08-09 13:00:00',
                'sort_start_date' => 20200809130000,
                'end_date' => '2020-08-16 17:00:00',
                'sort_end_date' => 20200816170000,
                'url' => NULL,
                'img' => '53.png',
                'html_body' => '<dd>

<h2 class="ttl_01 mb_S t_center">今週限定！アンケートに答えると1000円分無料でもらえる!!</h2>
<div class="area_01">
<p>今週限定で「好きなキャンペーン情報」をお答え頂ければ<br>
<span class="f_red f_bold">1000円分のポイントをプレゼント</span>致します!<br><br>
キャンペーンに参加した事ある会員様は今まで使った中で最も満足したキャンペーン<br>
参加した事がない会員様は興味があるキャンペーン<br>
でもOK!!<br>
<br>
<span class="f_red f_bold">上位にランクインしたキャンペーンは、次回特別価格にてご案内致します。</span>
</p>
<br>
「お問い合わせ」もしくは「help@g-stars.net」<br>
宛に<br>
<br>
・「好きなキャンペーン情報名」（予想師名）<br>
・「会員様のID」<br>
<br>
を明記の上送信して下さい。<br>
<br>


<h2 class="ttl_01 mb_S t_center">【注意事項】</h2>
<br><center><dt>・お一人様1回のみ有効<br>
<dt>・ポイントの付与は送信後2週間以内に行います<br>
<dt>・送信完了のメール、ポイント付与完了のメールは送信しない事がございますのでご了承ください。',
                'sort_date' => '2020-08-09',
                'created_at' => '2020-08-09 02:50:14',
                'updated_at' => '2020-08-09 02:53:26',
            ),
            42 => 
            array (
                'title' => '※上書き禁止※全額返還保証　フォーマット',
                'type' => 2,
                'open_flg' => 0,
                'link_flg' => 0,
                'groups' => NULL,
                'order_num' => 1,
                'start_date' => '2020-05-26 13:00:00',
                'sort_start_date' => 20200526130000,
                'end_date' => '9999-05-30 18:00:00',
                'sort_end_date' => 99990530180000,
                'url' => NULL,
                'img' => NULL,
                'html_body' => '<dd>
<h2 class="ttl_01 mb_S t_center">8/9(日)～8/16(日)限定保証</h2>

<div class="area_01 mb_LL">
<p>8/9(日)～8/16(日)にキャンペーン情報にご参加頂いた会員様の中で、万が一不的中だった場合は、次回弊社キャンペーン情報を<span class="f_red f_bold">半額</span>でご案内致します。
</div>

<h2 class="ttl_01 mb_S t_center">充実した保証制度</h2>
<h3 class="ttl_02 bl">■ 無期限保証</h3>
<div class="area_01">
<p>GOLDEN☆STARSではキャンペーン情報にご購入、ご参加頂いた会員様には<br>
充実した<span class="f_red f_bold">保証制度</span>がございます。<br><br>
万が一に備え、ご提供させて頂いたキャンペーン情報にて<b>不的中</b>だった場合、<br>
次回以降の央競馬開催に各日1鞍ずつの補填情報をご提供させて頂きます。<br>
<br>
補填情報を含めてご利用頂ければ<span class="f_red f_bold">大半の会員様が大幅なプラス収支!!</span>
</p>
<div class="box_01"><p class="f_bold">【!】ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供【!】</p></div>
</div>

<h3 class="ttl_02 bl">■ 新規登録会員様には10,000円分のポイントプレゼント!!</h3>
<div class="area_01 mb_LL">
<p>本登録後、すぐに使えるキャンペーンでの割引購入やサイト内にあるポイント情報閲覧に必要なポイントを</p>
<p class="f_red f_bold">10,000円分(100pt)必ずプレゼント!!</p>
</div>


<h2 class="ttl_01 mb_S t_center">Ｑ＆Ａ</h2>
<dl class="box_02">
<dt>【Ｑ】無期限保証制度は1回限りの提供なのでしょうか？</dt>
<dd>【Ａ】会員様のご利用状況によりますが、<br>
<span class="f_red f_bold">ご参加費用＋ご投資額分以上の的中をお届けするまで継続提供</span>させて頂きます。</dd>
<dt>【Ｑ】ポイントは有効期限がありますか？</dt>
<dd>【Ａ】基本的には弊社サービスが終了するまでご使用頂けます。</dd>
<dt>【Ｑ】登録したら請求がされるのでしょうか？</dt>
<dd>【Ａ】登録は無料です。キャンペーンご購入の際は有料となりますが、<br>
サイト内にある無料コンテンツにつきましては完全無料でご覧いただけます</dd>
</dl>





</dd>',
                'sort_date' => '2020-05-26',
                'created_at' => '2020-08-09 09:40:55',
                'updated_at' => '2020-08-09 09:40:55',
            ),
        ));
        
        
    }
}