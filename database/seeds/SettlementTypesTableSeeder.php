<?php

use Illuminate\Database\Seeder;

class SettlementTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        DB::connection('gsta')->table('settlement_types')->insert(array (
            0 => 
            array (
                'name' => 'TELECOM',
                'active' => 0,
                'clientip' => '00258',
                'netbank_clientip' => '',
                'sendid_length' => 16,
                'speed_credit_url' => 'https://secure.telecomcredit.co.jp/inetcredit/secure/one-click-order.pl',
                'credit_url' => 'https://secure.telecomcredit.co.jp/inetcredit/adult/order.pl',
                'netbank_url' => 'https://secure.telecomcredit.co.jp/banktransfer/settle/transfer_request.pl',
                'created_at' => '2020-03-09 06:39:37',
                'updated_at' => '2020-04-30 08:40:36',
            ),
            1 => 
            array (
                'name' => 'AXES',
                'active' => 1,
                'clientip' => '1011004183',
                'netbank_clientip' => '1081001763',
                'sendid_length' => 25,
                'speed_credit_url' => '',
                'credit_url' => 'https://gw.axes-payment.com/cgi-bin/credit/order.cgi',
                'netbank_url' => 'https://gw.axes-payment.com/cgi-bin/ebank.cgi',
                'created_at' => '2020-03-09 06:39:49',
                'updated_at' => '2020-04-30 08:40:36',
            ),
            2 => 
            array (
                'name' => 'CREDIX',
                'active' => 0,
                'clientip' => '1019000926',
                'netbank_clientip' => '',
                'sendid_length' => 25,
                'speed_credit_url' => '',
                'credit_url' => 'https://secure.credix-web.co.jp/cgi-bin/credit/order.cgi',
                'netbank_url' => '',
                'created_at' => '2020-03-09 06:40:06',
                'updated_at' => '2020-04-30 08:40:36',
            ),
        ));
        
        
    }
}