<?php

use Illuminate\Database\Seeder;

class Point_categoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$now_date = date("Y/m/d h:m:s");

		DB::connection('gsta')->table('point_categories')->insert([
			'name' => '通常',
			'remarks' => '通常',
			'created_at' => $now_date,
			'updated_at' => $now_date,
		]);
		DB::connection('gsta')->table('point_categories')->insert([
			'name' => '特別',
			'remarks' => '特別',
			'created_at' => $now_date,
			'updated_at' => $now_date,
		]);

		$pointSettings = [
				[
					'money' => 1000,
					'point' => 10
				],
				[
					'money' => 2000,
					'point' => 20
				],
				[
					'money' => 3000,
					'point' => 30
				],
				[
					'money' => 4000,
					'point' => 40
				],
				[
					'money' => 5000,
					'point' => 50
				],
		];

		$db_data = DB::connection('gsta')->table('point_categories')->get();
		foreach($db_data as $lines){
			foreach($pointSettings as $data){
				DB::connection('gsta')->table('point_settings')->insert([
					'category_id' => $lines->id,
					'money' => $data['money'],
					'point' => $data['point'],
					'created_at' => $now_date,
					'updated_at' => $now_date,
				]);
			}
		}
    }
}
