<?php

use Illuminate\Database\Seeder;

class Convert_tablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


    	$dataSet = [
    		[
    			'key' => '-%regist_url-',
				'value' => 'https://g-stars.net/registend',
				'memo' => '登録用URL'
    		],
    		[
    			'key' => '-%simple_login-',
				'value' => 'https://g-stars.net/member/home',
				'memo' => '簡単ログインURL'
    		],
    		[
    			'key' => '-%site_name-',
				'value' => 'GOLDEN★STARS',
				'memo' => 'サイト名'
    		],
    		[
    			'key' => '-%top_url-',
				'value' => 'https://g-stars.net/',
				'memo' => 'TOPページ'
    		],
    		[
    			'key' => '-%company_name-',
				'value' => 'ゴールデン★スターズ運営事務局',
				'memo' => '会社名'
    		],
    		[
    			'key' => '-%company_address-',
				'value' => '新宿区新宿4-1-22',
				'memo' => '会社住所'
    		],
    		[
    			'key' => '-%sekininsya-',
				'value' => '内藤 健太',
				'memo' => '運営責任者'
    		],
    		[
    			'key' => '-%info_mail-',
				'value' => 'help@g-stars.net',
				'memo' => 'インフォメールアドレス'
    		],
    		[
    			'key' => '-%company_navidial-',
				'value' => '0570-000282',
				'memo' => 'ナビダイヤル'
    		],
    		[
    			'key' => '-%mail_domain-',
				'value' => 'g-stars.net',
				'memo' => 'メール本文用ドメイン'
    		],
    		[
    			'key' => '-%keiba_race01-',
				'value' => '関屋記念（G3）小倉記念（G3）<br>',
				'memo' => '今週の注目レース'
    		],
    		[
    			'key' => '-%month_dividend-',
				'value' => '￥9,271,700',
				'memo' => 'ログイン前の前月の配当金'
    		],
    		[
    			'key' => '-%tipster_saddles-',
				'value' => '土曜日分　36鞍',
				'memo' => '鞍数'
    		],
    		[
    			'key' => '-%tipster_type-',
				'value' => '複勝',
				'memo' => '券種'
    		],
    		[
    			'key' => '-%pt_unit_price-',
				'value' => '1ポイント＝100円　※ポイントの有効期限は最大180日です',
				'memo' => null
    		],
    		[
    			'key' => '-%payment_method-',
				'value' => 'クレジット決済・銀行振込決済・ネットバンク決済',
				'memo' => null
    		],
    		[
    			'key' => '-%refund_policy-',
				'value' => 'デジタルコンテンツという商品の性格上、原則として返金には応じられません。',
				'memo' => null
    		],
    		[
    			'key' => '-%order_costs-',
				'value' => '通信料、お振込みの際の手数料がお客様のご負担となります。',
				'memo' => null
    		],
    		[
    			'key' => '-%domain-',
				'value' => 'g-stars.net',
				'memo' => null
    		]
    	];
    	foreach ($dataSet as $data) {
    		DB::connection('gsta')->table('convert_tables')->insert([
				'key' => $data['key'],
				'value' => $data['key'],
				'memo' => isset($data['key']) ?? null,
				'created_at' => date("Y/m/d h:m:s"),
				'updated_at' => date("Y/m/d h:m:s")
			]);
    	}
			
    }
}
