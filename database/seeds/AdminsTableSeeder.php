<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$listEmails = [
			'admin@dummy.com'
		];
        foreach ($listEmails as $email) {
            date_default_timezone_set('UTC');
            $create_date = date("Y/m/d h:m:s");
            $update_date = date("Y/m/d h:m:s");
            $last_date = date("Y/m/d h:m:s");

            DB::connection('mysql')->table('admins')->insert([
                'password' => bcrypt('12345678'),
                'email' => $email,
                'type' => '4',
                'remember_token' => session_create_id(),
                'select_db' => 'gsta',
                'access_host' => 'g-stars.net',
                'user_agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
                'created_at' => $create_date,
                'updated_at' => $update_date,
                'last_login_date' => $last_date
            ]);
        }
    }
}
