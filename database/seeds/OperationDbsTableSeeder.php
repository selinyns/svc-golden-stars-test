<?php

use Illuminate\Database\Seeder;

class OperationDbsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        DB::connection('mysql')->table('operation_dbs')->insert(array (
            0 => 
            array (
                'name' => 'GOLDEN★STARS',
                'db' => 'gsta',
                'host' => '192.168.0.100',
                'port' => '3306',
                'username' => 'db-username',
                'password' => 'db-password',
                'created_at' => '2020-01-10 15:57:07',
                'updated_at' => '2020-01-10 15:57:09',
            ),
        ));
        
        
    }
}