<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminsTableSeeder::class);
        $this->call(OperationDbsTableSeeder::class);
        
        $this->call(UsersTableSeeder::class);
        $this->call(ContentsTableSeeder::class);
        $this->call(Convert_tablesTableSeeder::class);
        $this->call(ForecastsTableSeeder::class);
        $this->call(Grant_pointsTableSeeder::class);
        $this->call(GroupsTableSeeder::class);
        $this->call(Landing_pagesTableSeeder::class);
        $this->call(Landing_pages_contentsTableSeeder::class);
        $this->call(Magnification_settingsTableSeeder::class);
        $this->call(Mail_contentsTableSeeder::class);
        $this->call(Point_categoriesTableSeeder::class);

        $this->call(TopContentsTableSeeder::class);
        $this->call(TopProductsTableSeeder::class);
        $this->call(SettlementTypesTableSeeder::class);
        $this->call(TipstersTableSeeder::class);
        $this->call(AchievementsTableSeeder::class);
        $this->call(MelmagaLogsTableSeeder::class);
    }
}
