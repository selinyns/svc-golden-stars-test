<?php

use App\Model\Payment_log;
use Illuminate\Database\Seeder;
use App\Model\Top_product;
use Carbon\Carbon;

class GeneratePurchaseLogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $numberOfDays = 30;
        $numberOfAdCode = 2;
        $created_at = date('Y-m-d');
        for ($generateAdCode = 0; $generateAdCode < $numberOfAdCode; $generateAdCode++) {
            $ad_cd = 'demo' . substr(str_shuffle('1234567890abcdefghijklmnopqrstuvwxyz'), 0, 8);
            $name = 'ハンサム-' . substr(str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'), 0, 8);
            $products = DB::connection('gsta')->table('top_products')->limit(100)->get();
            $products = $products->map(function ($entity) {
                return [
                    'id' => $entity->id,
                    'money' => $entity->money
                ];
            })->toArray();
            shuffle($products);
            $adCodes = [
                'group_id' => 6,
                'ad_cd' => $ad_cd,
                'single_opt' => 0,
                'agency_id' => 5,
                'category' => 1,
                'aggregate_flg' => 1,
                'name' => $name,
                'url' => 'https://g-stars.net/?afl=' . $ad_cd,
                'created_at' => $created_at,
                'updated_at' => $created_at
            ];
            echo '.';
            $productIndex = 0;
            DB::connection('gsta')->table('ad_codes')->insert($adCodes);
            for ($createPaymentLogs = 0; $createPaymentLogs <= $numberOfDays; $createPaymentLogs++, $productIndex++) {
                $currentDate = date('Y-m-d', strtotime('-' . $numberOfDays + $createPaymentLogs . ' day'));
                $statusTemp = rand(1, 2) == 1 ? 0 : 3;
                $status = rand(1, 7) == 1 ? 1 : $statusTemp;
                // $status = rand(1, 0);
                if (!isset($products[$productIndex]['id'])) {
                    break;
                }
                $loginId = rand(1, 999999);
                $paymentLogs = [
                    'agency_id' => 5,
                    'pay_type' => 1,
                    'login_id' => $loginId,
                    'type' => 0,
                    'product_id' => $products[$productIndex]['id'] . rand(111111, 999999),
                    'order_id' => '',
                    'money' => $products[$productIndex]['money'],
                    'point' => 0,
                    'ad_cd' => $ad_cd,
                    'status' => $status,
                    'sendid' => null,
                    'tel' => null,
                    'email' => null,
                    'username' => null,
                    'cont' => null,
                    'regist_date' => $currentDate . ' 07:00:00',
                    'pay_count' => 0,
                    'sort_date' => date('Ymd', strtotime($currentDate)),
                    'created_at' => $currentDate . ' 07:00:00',
                    'updated_at' => $currentDate . ' 07:00:00'
                ];

                $dayPvLogs = [
                    'ad_cd' =>$ad_cd,
                    'login_id' => $loginId,
                    'access_date' => date('Ymd', strtotime($currentDate)),
                    'created_at' => $currentDate . ' 07:00:00',
                    'updated_at'  => $currentDate . ' 07:00:00'
                ];
                $minusDays = 5;
                $perDay = $createPaymentLogs;
                $createPaymentLogs += $minusDays;
                DB::connection('gsta')->table('payment_logs')->insert($paymentLogs);
                DB::connection('gsta')->table('day_pv_logs')->insert($dayPvLogs);
                for ($createResultLogs = 0; $createResultLogs <= $minusDays; $createResultLogs++) {
                    $currentPerDay = date('Ymd', (strtotime('+' . $createResultLogs . ' day', strtotime($currentDate))));
                    $year = date('Y', strtotime($currentPerDay));
                    $month = date('m', strtotime($currentPerDay));
                    $day = date('d', strtotime($currentPerDay));

                    $start_date = "{$year}{$month}{$day}";
                    $end_date = "{$year}{$month}{$day}";

                    $yyyymm = "{$year}{$month}";

                    //サイトごとのDB名リストを取得
                    $listSiteDb = DB::select("select db from operation_dbs");

                    foreach ($listSiteDb as $lines) {
                        $db_name = $lines->db;

                        //仮登録人数
                        $temp_regist_data = DB::connection($db_name)->select(
                            "select ad_cd, count(ad_cd) as total from users where "
                                . "status = 0 and "
                                . "sort_temporary_datetime >= '{$start_date}000000' and "
                                . "sort_temporary_datetime <= '{$end_date}235959' "
                                . "group by ad_cd"
                        );

                        //登録人数
                        $regist_data = DB::connection($db_name)->select(
                            "select ad_cd, count(*) as total from users where "
                                . "status = 1 and "
                                . "regist_date >= '{$start_date}000000' and "
                                . "regist_date <= '{$end_date}235959' "
                                . "group by ad_cd"
                        );

                        //退会人数
                        $quite_data = DB::connection($db_name)->select(
                            "select ad_cd, count(*) as total from users where "
                                . "status = 2 and "
                                . "sort_quit_datetime >= '{$start_date}000000' and "
                                . "sort_quit_datetime <= '{$end_date}235959' "
                                . "group by ad_cd"
                        );

                        //注文数・合計金額
                        $order_data = DB::connection($db_name)->select("select ad_cd, count(*) as count, sum(money) amount from payment_logs where "
                            . "substring(sort_date, 1,6) = {$yyyymm} "
                            . "AND ad_cd = '{$ad_cd}'"
                            . "group by ad_cd");

                        //購入数・合計金額(購入)
                        $buy_data = DB::connection($db_name)->select("select ad_cd, count(*) as count, sum(money) amount from payment_logs where "
                            . "status in('0','3')  and "
                            . "substring(sort_date, 1,6) = {$yyyymm} "
                            . "AND ad_cd = '{$ad_cd}'"
                            . "group by ad_cd");

                        //広告ごとのアクセス数/アクティブ数
                        $ad_access_data = DB::connection($db_name)->select(
                            "select login_id,ad_cd,count(access_date) as pv from day_pv_logs where "
                                . "access_date = '{$start_date}' "
                                . "group by ad_cd, login_id"
                        );

                        $listData = [];

                        //仮登録者数
                        foreach ($temp_regist_data as $lines) {
                            $listData[$lines->ad_cd]['temp_reg'] = $lines->total;
                        }

                        //登録者数
                        foreach ($regist_data as $lines) {
                            $listData[$lines->ad_cd]['reg'] = $lines->total;
                        }

                        //退会者数
                        foreach ($quite_data as $lines) {
                            $listData[$lines->ad_cd]['quit'] = $lines->total;
                        }

                        //注文数
                        foreach ($order_data as $lines) {
                            $listData[$lines->ad_cd]['order'] = $lines->count;
                        }

                        if ($status == 0 || $status == 3) {
                            //購入数/売上金額
                            foreach ($buy_data as $lines) {
                                $listData[$lines->ad_cd]['pay'] = $lines->count;
                                $listData[$lines->ad_cd]['amount'] = $lines->amount;
                            }
                        }

                        //広告ごとのアクセス数/アクティブ数
                        foreach ($ad_access_data as $lines) {
                            if (empty($listData[$lines->ad_cd]['active'])) {
                                $listData[$lines->ad_cd]['active'] = 0;
                            }
                            $listData[$lines->ad_cd]['active']++;

                            if (empty($listData[$lines->ad_cd]['pv'])) {
                                $listData[$lines->ad_cd]['pv'] = 0;
                            }
                            $listData[$lines->ad_cd]['pv'] += $lines->pv;
                        }

                        //集計結果をyear_result_access_logsテーブルに登録
                        DB::connection($db_name)->transaction(function () use ($db_name, $listData, $start_date) {
                            $now_date = Carbon::now();
                            foreach ($listData as $ad_cd => $lines) {
                                if (empty($lines['temp_reg'])) {
                                    $lines['temp_reg'] = 0;
                                }
                                if (empty($lines['reg'])) {
                                    $lines['reg'] = 0;
                                }
                                if (empty($lines['quit'])) {
                                    $lines['quit'] = 0;
                                }
                                if (empty($lines['order'])) {
                                    $lines['order'] = 0;
                                }
                                if (empty($lines['pay'])) {
                                    $lines['pay'] = 0;
                                }
                                if (empty($lines['amount'])) {
                                    $lines['amount'] = 0;
                                }
                                if (empty($lines['active'])) {
                                    $lines['active'] = 0;
                                }
                                if (empty($lines['pv'])) {
                                    $lines['pv'] = 0;
                                }
                                DB::connection($db_name)->insert("insert ignore into result_ad_logs("
                                    . "ad_cd, "
                                    . "access_date, "
                                    . "pv, "
                                    . "temp_reg, "
                                    . "reg, "
                                    . "quit, "
                                    . "active, "
                                    . "order_num, "
                                    . "pay, "
                                    . "amount, "
                                    . "created_at, "
                                    . "updated_at) "
                                    . "values("
                                    . "'{$ad_cd}', "
                                    . "{$start_date}, "
                                    . "{$lines['pv']}, "
                                    . "{$lines['temp_reg']}, "
                                    . "{$lines['reg']}, "
                                    . "{$lines['quit']}, "
                                    . "{$lines['active']}, "
                                    . "{$lines['order']}, "
                                    . "{$lines['pay']}, "
                                    . "{$lines['amount']}, "
                                    . "'{$now_date}', "
                                    . "'{$now_date}') "
                                    . "on duplicate key update "
                                    . "pv = {$lines['pv']}, "
                                    . "temp_reg = {$lines['temp_reg']}, "
                                    . "reg = {$lines['reg']}, "
                                    . "quit = {$lines['quit']}, "
                                    . "active = {$lines['active']}, "
                                    . "order_num = {$lines['order']}, "
                                    . "pay = {$lines['pay']}, "
                                    . "amount = {$lines['amount']};");
                            }
                        });
                        if ($currentDate == date('Y-m-d')) {
                            break 2;
                        }
                        //使用しない昨日のデータをすべて削除
                        DB::connection($db_name)->transaction(function () use ($db_name, $start_date) {
                            DB::connection($db_name)->delete("delete from day_pv_logs where access_date < {$start_date}");
                        });
                    }
                }
            }
        }
    }
}
