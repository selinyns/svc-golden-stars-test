<?php

use Illuminate\Database\Seeder;

class MelmagaLogsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        DB::connection('gsta')->table('melmaga_logs')->insert(array (
            0 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => '次回キャンペーン発売のお知らせです。',
                'text_body' => 'こんにちは！
プレミアム担当飯島美穂です。

今から次回中央競馬キャンペーンの

『くぼ☆しょうご』

『パープルヘイズ』

こちらを土日両日分発売開始です！

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-

どちらも先週超高配当と言える結果を残しており、今週も大きな期待が寄せられています。

なんと言ってもくぼ☆しょうごは

‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
　土日合わせて５００万以上の配当を獲得
‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥

という驚愕の結果を残しています。

今後プレミアムを代表する情報元になるである事は間違いありません。

レベルアップしているこの精度を是非ご体感下さい。

━━━━━━━━━━━━━━━━━━━━
■『くぼ☆しょうご』

■詳細はコチラ▼
-%top_url-campaign.php?cid=27722&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『パープルヘイズ』

■詳細はコチラ▼
-%top_url-campaign.php?cid=27723&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『沈黙の密約』【エリート】

土曜日分　完売
日曜日分　あと3名様分で完売

■詳細はコチラ▼
-%top_url-campaign.php?cid=27712&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『TRANS UUUMANIZM～トランスウーマニズム～』

完売
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『アナーキー』３週間セット

受付終了　完売　
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『ヘルズ・ホース』

完売
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『女王様のス・ゴ・イ予想やで』

完売
━━━━━━━━━━━━━━━━━━━━
……………………………………………………
即完売必至
……………………………………………………
おかげさまでここ最近爆発的なご注文数を頂いており、コースによっては即完売の可能性も高くなっております。
ご注文頂いている場合でも未決済の状態で完売の場合は、ご参加頂く事ができません。
また完売後にご入金された場合、もしくは銀行の営業時間の関係上、完売の翌日以降にご入金が確認できた場合につきましても、完売していない他のコースに振替させて頂くか、ポイントにて返金させていただく場合がございます。
予めご了承ください。
━━━━━━━━━━━━━━━━━━━━
……………………………………………………
※ポイントをお持ちの方はポイント割引価格でご参加可能※
……………………………………………………
※ポイント割引でのご購入方法※
会員様の今までのポイント獲得状況によっては、
「商品購入」ページ
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
に※ポイント割引価格※と記載されている項目があります。
そちらがポイント割引後の価格ですので、※ポイント割引価格※と記載されて
いる項目にチェックを入れお申し込みください。
ポイントをお持ちなのにも関わらず※ポイント割引価格※と記載の項目がない
会員様はお手数ですが-%info_mail-宛に「ポイント割引希望」と明記の上、メ
ールを送信してください。
担当が折り返しご連絡させて頂きます。
また、場合によっては「会員様特別割引」等、ポイント割引よりもさらにお得な割引の項目が出ている場合があります。
そちらはお持ちのポイントをお使い頂けなくても特別割引価格でご参加頂く事ができます。
もちろん通常価格でも割引価格でも、ご提供内容は全く同一のものとなります。
━━━━━━━━━━━━━━━━━━━━
※完売済みの商品は表示がなくなる、または「0円」と表記されておりますが、ご注文頂いてもご参加頂けません。
また完売した商品はご注文済みでも未決済の会員様はご参加頂く事ができませんのでご了承ください。
----------------------------------------
-%site_name-
■会員様用ページはコチラ▼
http://www.premium-h.jp/login_top.php?aK=-%accessKey-
----------------------------
▼あなた様のログイン情報▼
会員ID:-%login_id-
パスワード:-%password-。
----------------------------
　【お問い合わせ 配信停止】
　　-%info_mail-
--------------------------------------------
Copyright(C)2010-2020 Premium All Right Reserved.',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-26 18:42:30',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200526184200,
                'created_at' => '2020-05-26 09:42:30',
                'updated_at' => '2020-05-26 09:42:30',
            ),
            1 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'help@g-stars.net',
                'subject' => '次回キャンペーン発売のお知らせです。',
                'text_body' => 'こんにちは！
プレミアム担当飯島美穂です。

今から次回中央競馬キャンペーンの

『くぼ☆しょうご』

『パープルヘイズ』

こちらを土日両日分発売開始です！

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-

どちらも先週超高配当と言える結果を残しており、今週も大きな期待が寄せられています。

なんと言ってもくぼ☆しょうごは

‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥
　土日合わせて５００万以上の配当を獲得
‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥‥

という驚愕の結果を残しています。

今後プレミアムを代表する情報元になるである事は間違いありません。

レベルアップしているこの精度を是非ご体感下さい。

━━━━━━━━━━━━━━━━━━━━
■『くぼ☆しょうご』

■詳細はコチラ▼
-%top_url-campaign.php?cid=27722&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『パープルヘイズ』

■詳細はコチラ▼
-%top_url-campaign.php?cid=27723&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『沈黙の密約』【エリート】

土曜日分　完売
日曜日分　あと3名様分で完売

■詳細はコチラ▼
-%top_url-campaign.php?cid=27712&aK=-%accessKey-

■お申込みはコチラ▼
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『TRANS UUUMANIZM～トランスウーマニズム～』

完売
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『アナーキー』３週間セット

受付終了　完売　
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『ヘルズ・ホース』

完売
━━━━━━━━━━━━━━━━━━━━
━━━━━━━━━━━━━━━━━━━━
■『女王様のス・ゴ・イ予想やで』

完売
━━━━━━━━━━━━━━━━━━━━
……………………………………………………
即完売必至
……………………………………………………
おかげさまでここ最近爆発的なご注文数を頂いており、コースによっては即完売の可能性も高くなっております。
ご注文頂いている場合でも未決済の状態で完売の場合は、ご参加頂く事ができません。
また完売後にご入金された場合、もしくは銀行の営業時間の関係上、完売の翌日以降にご入金が確認できた場合につきましても、完売していない他のコースに振替させて頂くか、ポイントにて返金させていただく場合がございます。
予めご了承ください。
━━━━━━━━━━━━━━━━━━━━
……………………………………………………
※ポイントをお持ちの方はポイント割引価格でご参加可能※
……………………………………………………
※ポイント割引でのご購入方法※
会員様の今までのポイント獲得状況によっては、
「商品購入」ページ
http://www.premium-h.jp/settlement_list.php?aK=-%accessKey-
に※ポイント割引価格※と記載されている項目があります。
そちらがポイント割引後の価格ですので、※ポイント割引価格※と記載されて
いる項目にチェックを入れお申し込みください。
ポイントをお持ちなのにも関わらず※ポイント割引価格※と記載の項目がない
会員様はお手数ですが-%info_mail-宛に「ポイント割引希望」と明記の上、メ
ールを送信してください。
担当が折り返しご連絡させて頂きます。
また、場合によっては「会員様特別割引」等、ポイント割引よりもさらにお得な割引の項目が出ている場合があります。
そちらはお持ちのポイントをお使い頂けなくても特別割引価格でご参加頂く事ができます。
もちろん通常価格でも割引価格でも、ご提供内容は全く同一のものとなります。
━━━━━━━━━━━━━━━━━━━━
※完売済みの商品は表示がなくなる、または「0円」と表記されておりますが、ご注文頂いてもご参加頂けません。
また完売した商品はご注文済みでも未決済の会員様はご参加頂く事ができませんのでご了承ください。
----------------------------------------
-%site_name-
■会員様用ページはコチラ▼
http://www.premium-h.jp/login_top.php?aK=-%accessKey-
----------------------------
▼あなた様のログイン情報▼
会員ID:-%login_id-
パスワード:-%password-。
----------------------------
　【お問い合わせ 配信停止】
　　-%info_mail-
--------------------------------------------
Copyright(C)2010-2020 Premium All Right Reserved.',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-26 18:44:17',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200526184400,
                'created_at' => '2020-05-26 09:44:16',
                'updated_at' => '2020-05-26 09:44:17',
            ),
            2 => 
            array (
                'send_status' => 2,
                'send_count' => 5,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => '配信テスト',
                'text_body' => '配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:35',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529104000,
                'created_at' => '2020-05-29 01:40:13',
                'updated_at' => '2020-05-29 09:21:36',
            ),
            3 => 
            array (
                'send_status' => 2,
                'send_count' => 6,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'テスト配信',
                'text_body' => 'テスト配信',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:33',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529104200,
                'created_at' => '2020-05-29 01:42:43',
                'updated_at' => '2020-05-29 09:21:35',
            ),
            4 => 
            array (
                'send_status' => 2,
                'send_count' => 6,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'テスト３',
                'text_body' => 'テスト３',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:32',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529104600,
                'created_at' => '2020-05-29 01:46:21',
                'updated_at' => '2020-05-29 09:21:33',
            ),
            5 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => '配信テストです',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141769",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:29',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529110300,
                'created_at' => '2020-05-29 02:03:11',
                'updated_at' => '2020-05-29 09:21:29',
            ),
            6 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト１',
                'text_body' => 'メルマガ配信テスト１',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141769",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:30',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529110300,
                'created_at' => '2020-05-29 02:03:54',
                'updated_at' => '2020-05-29 09:21:31',
            ),
            7 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141769",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:27',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529145100,
                'created_at' => '2020-05-29 05:51:31',
                'updated_at' => '2020-05-29 09:21:28',
            ),
            8 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141769",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:26',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529151500,
                'created_at' => '2020-05-29 06:15:50',
                'updated_at' => '2020-05-29 09:21:26',
            ),
            9 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141769",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:24',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529151800,
                'created_at' => '2020-05-29 06:18:47',
                'updated_at' => '2020-05-29 09:21:25',
            ),
            10 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:23',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529152400,
                'created_at' => '2020-05-29 06:24:46',
                'updated_at' => '2020-05-29 09:21:23',
            ),
            11 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:21',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529154100,
                'created_at' => '2020-05-29 06:41:11',
                'updated_at' => '2020-05-29 09:21:22',
            ),
            12 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:20',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529173700,
                'created_at' => '2020-05-29 08:37:25',
                'updated_at' => '2020-05-29 09:21:20',
            ),
            13 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 17:43:45',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529174300,
                'created_at' => '2020-05-29 08:43:45',
                'updated_at' => '2020-05-29 08:43:46',
            ),
            14 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:18',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529174700,
                'created_at' => '2020-05-29 08:47:36',
                'updated_at' => '2020-05-29 09:21:19',
            ),
            15 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:17',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529174900,
                'created_at' => '2020-05-29 08:49:30',
                'updated_at' => '2020-05-29 09:21:17',
            ),
            16 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:15',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529175000,
                'created_at' => '2020-05-29 08:50:50',
                'updated_at' => '2020-05-29 09:21:16',
            ),
            17 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:14',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529175300,
                'created_at' => '2020-05-29 08:53:51',
                'updated_at' => '2020-05-29 09:21:14',
            ),
            18 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:12',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529175400,
                'created_at' => '2020-05-29 08:54:58',
                'updated_at' => '2020-05-29 09:21:12',
            ),
            19 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 17:56:18',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529175600,
                'created_at' => '2020-05-29 08:56:18',
                'updated_at' => '2020-05-29 08:56:18',
            ),
            20 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 17:56:35',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529175600,
                'created_at' => '2020-05-29 08:56:35',
                'updated_at' => '2020-05-29 08:56:35',
            ),
            21 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:09',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529180100,
                'created_at' => '2020-05-29 09:01:11',
                'updated_at' => '2020-05-29 09:21:09',
            ),
            22 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:11',
                'reserve_send_date' => '2020-05-29 18:00:00',
                'sort_reserve_send_date' => 20200529180000,
                'created_at' => '2020-05-29 09:04:21',
                'updated_at' => '2020-05-29 09:21:11',
            ),
            23 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:08',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529180500,
                'created_at' => '2020-05-29 09:05:40',
                'updated_at' => '2020-05-29 09:21:08',
            ),
            24 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:06',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529180700,
                'created_at' => '2020-05-29 09:07:38',
                'updated_at' => '2020-05-29 09:21:06',
            ),
            25 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:09:42',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529180900,
                'created_at' => '2020-05-29 09:09:42',
                'updated_at' => '2020-05-29 09:09:42',
            ),
            26 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:05',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529181100,
                'created_at' => '2020-05-29 09:11:58',
                'updated_at' => '2020-05-29 09:21:05',
            ),
            27 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:03',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529181300,
                'created_at' => '2020-05-29 09:13:46',
                'updated_at' => '2020-05-29 09:21:03',
            ),
            28 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:02',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529181600,
                'created_at' => '2020-05-29 09:16:07',
                'updated_at' => '2020-05-29 09:21:02',
            ),
            29 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:22:02',
                'reserve_send_date' => '2020-05-29 18:22:00',
                'sort_reserve_send_date' => 20200529182200,
                'created_at' => '2020-05-29 09:20:48',
                'updated_at' => '2020-05-29 09:22:02',
            ),
            30 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:21:08',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529182100,
                'created_at' => '2020-05-29 09:21:07',
                'updated_at' => '2020-05-29 09:21:08',
            ),
            31 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:25:01',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529182400,
                'created_at' => '2020-05-29 09:24:02',
                'updated_at' => '2020-05-29 09:25:02',
            ),
            32 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:27:02',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529182600,
                'created_at' => '2020-05-29 09:26:22',
                'updated_at' => '2020-05-29 09:27:02',
            ),
            33 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:40:06',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529183500,
                'created_at' => '2020-05-29 09:35:32',
                'updated_at' => '2020-05-29 09:40:06',
            ),
            34 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:40:04',
                'reserve_send_date' => '2020-05-29 18:36:00',
                'sort_reserve_send_date' => 20200529183600,
                'created_at' => '2020-05-29 09:35:57',
                'updated_at' => '2020-05-29 09:40:04',
            ),
            35 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:40:03',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529183800,
                'created_at' => '2020-05-29 09:38:27',
                'updated_at' => '2020-05-29 09:40:03',
            ),
            36 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:40:01',
                'reserve_send_date' => '2020-05-29 18:39:00',
                'sort_reserve_send_date' => 20200529183900,
                'created_at' => '2020-05-29 09:38:52',
                'updated_at' => '2020-05-29 09:40:01',
            ),
            37 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:40:10',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529184000,
                'created_at' => '2020-05-29 09:40:10',
                'updated_at' => '2020-05-29 09:40:10',
            ),
            38 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:53:57',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200529185300,
                'created_at' => '2020-05-29 09:53:57',
                'updated_at' => '2020-05-29 09:53:57',
            ),
            39 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:55:02',
                'reserve_send_date' => '2020-05-29 18:00:00',
                'sort_reserve_send_date' => 20200529180000,
                'created_at' => '2020-05-29 09:54:16',
                'updated_at' => '2020-05-29 09:55:02',
            ),
            40 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト',
                'text_body' => 'メルマガ配信テスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-05-29 18:56:02',
                'reserve_send_date' => '2020-05-29 18:55:00',
                'sort_reserve_send_date' => 20200529185500,
                'created_at' => '2020-05-29 09:55:02',
                'updated_at' => '2020-05-29 09:56:02',
            ),
            41 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'test',
                'text_body' => 'test',
                'html_body' => 'test',
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "system@s-vc.page",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 09:04:24',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200601090400,
                'created_at' => '2020-06-01 00:04:24',
                'updated_at' => '2020-06-01 00:04:25',
            ),
            42 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'te1',
                'text_body' => 'te1',
                'html_body' => 'te1',
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`mail_address` in (?) and (`users`.`group_id` in (?)) and `users`.`status` = ?',
                'bindings' => '1,system@s-vc.page,7,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "system@s-vc.page",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 09:15:01',
                'reserve_send_date' => '2020-06-01 09:15:00',
                'sort_reserve_send_date' => 20200601091500,
                'created_at' => '2020-06-01 00:07:33',
                'updated_at' => '2020-06-01 00:15:02',
            ),
            43 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト11',
                'text_body' => 'メルマガ配信テスト11',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 11:23:14',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200601112300,
                'created_at' => '2020-06-01 02:23:14',
                'updated_at' => '2020-06-01 02:23:14',
            ),
            44 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト11123',
                'text_body' => 'メルマガ配信テスト11123',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 11:26:01',
                'reserve_send_date' => '2020-06-01 11:26:00',
                'sort_reserve_send_date' => 20200601112600,
                'created_at' => '2020-06-01 02:24:21',
                'updated_at' => '2020-06-01 02:26:02',
            ),
            45 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テストsdsdsd',
                'text_body' => 'メルマガ配信テストsdsdsd',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 11:24:58',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200601112400,
                'created_at' => '2020-06-01 02:24:57',
                'updated_at' => '2020-06-01 02:24:58',
            ),
            46 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'メルマガ配信テスト111111122',
                'text_body' => 'メルマガ配信テスト111111122',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and `users`.`id` in (?) and (`users`.`group_id` is null or `users`.`group_id` in (?, ?, ?, ?)) and `users`.`status` = ?',
                'bindings' => '1,141758,6,7,8,9,1',
                'items' => '{
"search_item": "\\u9867\\u5ba2ID",
"search_value": "141758",
"search_type": "\\u542b\\u3080",
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-01 11:30:01',
                'reserve_send_date' => '2020-06-01 11:30:00',
                'sort_reserve_send_date' => 20200601113000,
                'created_at' => '2020-06-01 02:25:24',
                'updated_at' => '2020-06-01 02:30:01',
            ),
            47 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'help@g-stars.net',
                'subject' => 'テスト',
                'text_body' => 'テストテストテスト',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-02 12:02:12',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200602120200,
                'created_at' => '2020-06-02 03:02:12',
                'updated_at' => '2020-06-02 03:02:13',
            ),
            48 => 
            array (
                'send_status' => 2,
                'send_count' => 1,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'help@g-stars.net',
                'subject' => 'て',
                'text_body' => 'てて',
                'html_body' => NULL,
                'newspage_flg' => 1,
            'query' => 'select distinct `users`.`id`, `users`.`mail_address` from `users` left join `payment_logs` on `users`.`login_id` = `payment_logs`.`login_id` where `users`.`mail_status` = ? and (`users`.`group_id` in (?)) and `users`.`status` = ?',
                'bindings' => '1,8,1',
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-02 12:03:01',
                'reserve_send_date' => '2020-06-02 12:03:00',
                'sort_reserve_send_date' => 20200602120300,
                'created_at' => '2020-06-02 03:02:50',
                'updated_at' => '2020-06-02 03:03:02',
            ),
            49 => 
            array (
                'send_status' => 1,
                'send_count' => 4,
                'send_method' => NULL,
            'from_name' => 'GOLDEN★STARS(ゴールデン★スターズ)',
                'from_mail' => 'info@g-stars.net',
                'subject' => 'テストです！',
                'text_body' => 'テストです',
                'html_body' => NULL,
                'newspage_flg' => 1,
                'query' => NULL,
                'bindings' => NULL,
                'items' => '{
"status": "\\u672c\\u767b\\u9332"
}',
                'send_date' => '2020-06-02 15:40:02',
                'reserve_send_date' => NULL,
                'sort_reserve_send_date' => 20200602153900,
                'created_at' => '2020-06-02 06:39:28',
                'updated_at' => '2020-06-02 06:40:03',
            ),
        ));
        
        
    }
}