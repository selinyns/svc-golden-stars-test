# Stop apache
sudo systemctl stop httpd
sudo yum remove php-cli mod_php php-common -y
sudo yum clean all
sudo amazon-linux-extras | grep php
sudo amazon-linux-extras disable php7.2
sudo amazon-linux-extras enable php7.1
sudo yum clean metadata
sudo amazon-linux-extras install -y php7.1
sudo yum install php-mbstring -y
sudo yum install php-xml -y
sudo yum install php-intl -y
sudo yum install php-zip -y
sudo yum install zip -y
cd /var/www/html/svc
sudo composer update
sudo systemctl start httpd
echo -e "\e[92mSuccessfuly install PHP 7.1.33 Done!!  \e[0m"
exit 0;