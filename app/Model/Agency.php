<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Agency extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
		'id',
		'name',
		'login_id',
		'password',
		'password_raw',
		'remember_token',
		'memo',
		'created_at',
		'updated_at'
	];

	public function __construct($attributes = array())
	{
		parent::__construct($attributes);
		// Get gsta database
		$db = config('database.connections.gsta.database');
		// Identify if exist
		if(isset($db) && !empty($db) && !is_null($db)){
			$this->connection = $db;
		} else {
			$this->connection = 'gsta';
		}
	}
}
