//--------------------------------
// メールアドレスチェック
//--------------------------------
$(function(){
	$('form').submit(function(){
		//初期化
		var errorFlag = false;
		$('.icloud_error, .address_error').css('display','none');

		//メールアドレスの取得
		var mail = $('input[name="mail_address"]', this).val();

		//アドレスミスチェック
		if(!mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
			errorFlag = true;
			$('.address_error').fadeIn();
		}
		//gmail,yahooチェック
		else if(!mail.match(/gmail.com|yahoo.co.jp/)){
			errorFlag = true;
			$('.freemail_error').fadeIn();
		}
		
		//エラーの際の処理
		if(errorFlag == true){
			return false;
		}
	});
	//閉じる
	$('.btn_close').click(function(){
		$('.overlay_01').fadeOut();
	});
});