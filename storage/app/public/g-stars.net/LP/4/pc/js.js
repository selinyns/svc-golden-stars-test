//--------------------------------
// メールアドレスチェック
//--------------------------------
$(function(){
	$('form').submit(function(){
		//初期化
		var errorFlag = false;
		$('.icloud_error, .address_error').css('display','none');

		//メールアドレスの取得
		var mail = $('input[name="mail_address"]', this).val();

		//アドレスミスチェック
		if(!mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
			errorFlag = true;
			$('.address_error').fadeIn();
		}
		//iCloudチェック
		if(mail.match(/icloud.com/)){
			errorFlag = true;
			$('.icloud_error').fadeIn();
		}

		//エラーの際の処理
		if(errorFlag == true){
			return false;
		}
	});
	//閉じる
	$('.btn_close').click(function(){
		$('.overlay_01').fadeOut();
	});
});
//--------------------------------
//金額に単位を付ける
//--------------------------------
$(function(){
   $('.ticket > span.pay').each(function(){
        var num = $(this).html(); // 値を取得
        var digit = ['', '万', '億', '兆']; // 数字の桁を配列で定義
        var result = '';
        if(num){
            var nums = String(num).replace(/(\d)(?=(\d\d\d\d)+$)/g, "$1,").split(",").reverse();
            // 先頭に0がある場合は削除して、数字の単位を付け加える（4桁すべて0の場合は何もしない）
            for(var i=0;i < nums.length;i++){
                if(!nums[i].match(/^[0]+$/)){
                    nums[i] = nums[i].replace(/^[0]+/g, "");// 頭の0を削除
                    result = nums[i] + digit[i] + result;
                }
            }
        }
	$(this).html(result + "円");
   });
});

//--------------------------------
// カウントダウン
//--------------------------------
// 期限時間の設定
var gDayOfWeek = 0, //曜日 0(日曜)～6(土曜)
    gHour = 12, // 時
    gMinute = 0, // 分
    gSecond = 0; // 秒

// その他使用する変数の宣言
var cDay, cHour, cMinute, cSecond,
    insert = "";

// 2桁に変換
function addZero(num){
  return ('0' + num).slice(-2);
};

// カウントダウンの処理
function countdown() {

    var today = new Date();
    var todayOfWeek = today.getDay();//今日の曜日 0(日曜)~6(土曜)

    // ゴール時間セット
    var goal = new Date(today.getFullYear(),today.getMonth(),today.getDate(),gHour,gMinute,gSecond);


    if(todayOfWeek == gDayOfWeek && today.getHours() < gHour){//今日が設定曜日の期限時間前なら
        goal.setDate(today.getDate());//日付はそのまま
    }else if(todayOfWeek >= gDayOfWeek){
        goal.setDate(today.getDate() + (7+(gDayOfWeek-todayOfWeek)));
    }else{
        goal.setDate(today.getDate() + (gDayOfWeek-todayOfWeek));
    }

    // 現在から期限日までの差を取得
    var diff = goal.getTime() - today.getTime();

    // 日数を取得
    cDay = Math.floor(diff / (1000 * 60 * 60 * 24));
    diff -= (cDay * (1000 * 60 * 60 * 24));

    // 時間を取得
    cHour = Math.floor(diff / (1000 * 60 * 60));
    diff -= (cHour * (1000 * 60 * 60));

    // 分を取得
    cMinute = Math.floor(diff / (1000 * 60));
    diff -= (cMinute * (1000 * 60));

    // 秒を取得
    cSecond = Math.floor(diff / (1000));
    diff -= (cSecond * (1000));

    // フレーム
    cFlame = Math.floor(diff / (10));

    // 残り日数の書き換え
    insert = "";
    insert += '<span class="d">' + cDay + '</span>' + "日";
    insert += '<span class="h">' + addZero(cHour) + '</span>' + "時間";
    insert += '<span class="m">' + addZero(cMinute) + '</span>' + "分";
    insert += '<span class="s">' + addZero(cSecond) + '</span>' + "秒";
    insert += '<span class="f">' + addZero(cFlame) + '</span>';

    var target = document.querySelectorAll('.cd_time');
    for(var i=0; i<target.length; i++){
        target[i].innerHTML = insert;
    }

    // カウントダウンの処理を再実行
    setTimeout(countdown, 10);

}

// 処理の実行
countdown();