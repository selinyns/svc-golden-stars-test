#コピーを除外するファイル

#GIT
.gitignore
.gitattributes

#SVN
.svn

#Bladeテンプレートをコンパイルしたものやセッションのファイル、キャッシュファイル、その他フレームワークが作り出したファイル。
storage

#アップロード画像フォルダ
upload_images

#会員の声の画像フォルダ
public/images/voice

#トップコンテンツの画像フォルダ
top_content

#ランディングページへのシンボリックリンク
LP

#顧客データアップロードフォルダ
upload

#ENV(設定ファイル)
.env.testing

#bootstrap
bootstrap/cache/config.php


