//--------------------------------
// メールアドレスチェック
//--------------------------------
$(function(){
	$('form').submit(function(){
		//初期化
		var errorFlag = false;
		$('.icloud_error, .address_error').css('display','none');

		//メールアドレスの取得
		var mail = $('input[name="mail_address"]', this).val();

		//アドレスミスチェック
		if(!mail.match(/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/)){
			errorFlag = true;
			$('.address_error').fadeIn();
		}
		//iCloudチェック
		if(mail.match(/icloud.com/)){
			errorFlag = true;
			$('.icloud_error').fadeIn();
		}
		
		//エラーの際の処理
		if(errorFlag == true){
			return false;
		}
	});
	//閉じる
	$('.btn_close').click(function(){
		$('.overlay_01').fadeOut();
	});
});

//--------------------------------
// 表示領域でアニメーション制御（jquery.inview.min.js）
//--------------------------------
$(function() {
	$('.js-inview').on('inview', function(event, isInView) {
		if (isInView) {
            if($(this).attr('data-delay') != undefined){
                var delayVal = $(this).attr('data-delay');
                    $(this).delay(delayVal).queue(function(){
                        $(this).addClass('active');
                        $(this).dequeue();
                });
            }else{
                $(this).delay(300).queue(function(){
                    $(this).addClass('active');
                    $(this).dequeue();
                });
            }
		}
	});
});

//--------------------------------
// スライダー
//--------------------------------
$(function() {
$('.js-slides').slick({
  autoplay:true,
  autoplaySpeed:3000,
  dots:true,
  centerMode: true,
  centerPadding: '210px',
  slidesToShow: 1,
  arrows:false,
  responsive: [{
      breakpoint: 999,
      settings: {
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
  }]
});
});

//--------------------------------
// 金額に単位を付ける
//--------------------------------
$(function(){
   $('.list_hit p.dividend').each(function(){
        var num = $(this).html(); // 値を取得
        var digit = ['', '万', '億', '兆']; // 数字の桁を配列で定義
        var result = '';
        if(num){
            var nums = String(num).replace(/(\d)(?=(\d\d\d\d)+$)/g, "$1,").split(",").reverse();
            // 先頭に0がある場合は削除して、数字の単位を付け加える（4桁すべて0の場合は何もしない）
            for(var i=0;i < nums.length;i++){
                if(!nums[i].match(/^[0]+$/)){
                    nums[i] = nums[i].replace(/^[0]+/g, "");// 頭の0を削除
                    result = nums[i] + digit[i] + result;
                }
            }
        }
	$(this).html(result + "<span>円</span>");
   });
});