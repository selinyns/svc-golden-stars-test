//--------------------------------
// ドメイン別表示切替
//--------------------------------
$(function(){
	$('.mail_confirm, .area_retry').css('display','none');//初期非表示

	//アドレスを取得
	var mail = $('#mail_address').html();

	//gmailチェック
	if(mail.match(/gmail.com/)){
		$('#area_google').css('display','block');
	}
	//yahooチェック
	else if(mail.match(/yahoo.co.jp/)){
		$('#area_yahoo').css('display','block');
	}
	//キャリア他
	else{
		$('#area_other').css('display','block');
		$('.area_retry').css('display','block');
	}
});
//--------------------------------
// コピーボタン
//--------------------------------
$(function(){
	var copyEmailBtn = document.querySelector('.js-copybtn');
	copyEmailBtn.addEventListener('click', function(event) {
 	   // .js-copytextのテキストを選択
	    var copyText = document.querySelector('.js-copytext');
	    var range = document.createRange();
	    range.selectNode(copyText);
 	   window.getSelection().addRange(range);
 	   try {
 	       // テキストを選択したらクリップボードにコピーする
  	      var successful = document.execCommand('copy');
  	      var msg = successful ? 'successful' : 'unsuccessful';
 	       console.log('Copy command was ' + msg);
	        alert('コピーしました');
 	   } catch(err) {
 	       console.log('Oops, unable to copy');
 	   }
 	   // 選択状態を解除する
 	   window.getSelection().removeAllRanges();
	});
});